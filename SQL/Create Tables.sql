-- -- DROP TABLE

-- DROP TABLE rentaguide.aim;

CREATE TABLE rentaguide.aim (
	id serial NOT NULL,
	aim varchar(255) NULL,
	aim_detail varchar(255) NULL,
	CONSTRAINT aim_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.guide;

CREATE TABLE rentaguide.guide (
	id serial NOT NULL,
	driver_licence varchar(255) NULL,
	gender varchar(255) NULL,
	mail varchar(255) NULL,
	"name" varchar(255) NULL,
	"password" varchar(255) NULL,
	phone varchar(255) NULL,
	picture varchar(255) NULL,
	profile varchar(255) NULL,
	register_date timestamp NULL,
	surname varchar(255) null,
	CONSTRAINT guide_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.guide_aim;

CREATE TABLE rentaguide.guide_aim (
	guide_id int4 NOT NULL,
	aim_id int4 NOT NULL
);

-- -- DROP TABLE

-- -- DROP TABLE rentaguide.guide_language;

CREATE TABLE rentaguide.guide_language (
	guide_id int4 NOT NULL,
	language_id int4 NOT NULL
);

-- -- DROP TABLE

-- -- DROP TABLE rentaguide.guide_place;

CREATE TABLE rentaguide.guide_place (
	guide_id int4 NOT NULL,
	place_id int4 NOT NULL
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.guide_price;

CREATE TABLE rentaguide.guide_price (
	id serial NOT NULL,
	currency varchar(255) NULL,
	driving_price float8 NULL,
	guiding_price float8 NULL,
	"time" varchar(255) NULL,
	guide_id int4 NULL,
	place_id int4 NULL,
	CONSTRAINT guide_price_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.index_search;

CREATE TABLE rentaguide.index_search (
	id serial NOT NULL,
	aim_id int4 NULL,
	place_id int4 NULL,
	checkin date NULL,
	checkout date NULL,
	search_date date NULL,
	gender varchar(255) NULL,
	driver_licence varchar(255) NULL,
	max_price int4 NULL,
	min_price int4 NULL,
	currency varchar(255) NULL,
	CONSTRAINT index_search_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide."language";

CREATE TABLE rentaguide."language" (
	id serial NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT language_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.place;

CREATE TABLE rentaguide.place (
	id serial NOT NULL,
	country varchar(255) NULL,
	district varchar(255) NULL,
	province varchar(255) NULL,
	CONSTRAINT place_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.reservation;

CREATE TABLE rentaguide.reservation (
	id serial NOT NULL,
	aim_id int4 NULL,
	guide_id int4 NULL,
	place_id int4 NULL,
	timesheet_id int4 NULL,
	user_id int4 NULL,
	start_date timestamp NULL,
	end_date timestamp NULL,
	notes varchar(255) NULL,
	reservation_status varchar(255) NULL,
	total_price float8 NULL,
	CONSTRAINT reservation_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.timesheet;

CREATE TABLE rentaguide.timesheet (
	id serial NOT NULL,
	end_date timestamp NULL,
	guide_id int4 NULL,
	place_id int4 NULL,
	start_date timestamp NULL,
	status varchar(255) NULL,
	CONSTRAINT timesheet_pkey PRIMARY KEY (id)
);

-- -- DROP TABLE

-- DROP TABLE rentaguide.users;

CREATE TABLE rentaguide.users (
	id serial NOT NULL,
	mail varchar(255) NULL,
	"name" varchar(255) NULL,
	passport_no varchar(255) NULL,
	"password" varchar(255) NULL,
	phone varchar(255) NULL,
	register_date timestamp NULL,
	surname varchar(255) NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

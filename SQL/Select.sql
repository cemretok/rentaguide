select
	g.id guide_id,
	gpl.place_id,
	g.gender,
	(case
		when '1' = ? 
		then
		(case
			when (
				select count(1)
			from
				rentaguide.users u
			where
				u.id = ? ) > 0 then (gp.guiding_price + gp.driving_price)* rate
			else gp.guiding_price + gp.driving_price
		end )
		else
		(case
			when (
				select count(1)
			from
				rentaguide.users u
			where
				u.id = ? ) > 0 then gp.guiding_price* rate
			else gp.guiding_price
		end )
	end) g_price
from
	guide g,
	guide_place gpl,
	guide_price gp,
	guide_language gl
where
	1 = 1
	and g.id = gpl.guide_id
	and gp.guide_id = g.id
	and gp.place_id = gpl.place_id
	and (gpl.place_id = ?
	or ? = 0)
	and exists (
	select
		1
	from
		guide_aim ga,
		aim a
	where
		a.id = ga.aim_id
		and ga.guide_id = g.id
		and (a.id = (?)
		or 0 = ?) )
	and exists(
	select
		1
	from
		place p,
		guide_place gp
	where
		p.id = gp.place_id
		and gp.guide_id = g.id
		and (p.id = ?
		or 0 = ? ) )
	and not exists(
	select
		1
	from
		reservation r
	where
		r.guide_id = g.id
		and r.reservation_status = 'belirlenecek' )
	and not exists(
	select
		1
	from
		timesheet t
	where
		t.guide_id = g.id
		and t.status = 'belirlenecek'
		and (? between t.start_date and t.end_date
		or ? between t.start_date and t.end_date
		or t.start_date between ? and ?
		or t.end_date between ? and ?))
	and exists(
	select
		1
	from
		guide_price gp,
		place p
	where
		gp.guide_id = g.id
		and p.id = gp.place_id
		and (gp.guiding_price >= ?
		or ? is null)
		and (gp.guiding_price <= ?
		or ? is null))
	and ( g.gender = ?
	or ? = '0')
	and g.driver_licence = ?
	and gl.language_id in (?)
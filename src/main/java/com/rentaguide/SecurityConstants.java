package com.rentaguide;

public class SecurityConstants {

    public static final String USER_SIGN_UP_URL = "/user/signup";
    public static final String MAIN = "/";
    public static final String LOGIN = "/login";
    public static final String GUIDE_SIGN_UP_URL = "/guide/signup";
    public static final String PLACE_GET_ALL_URL = "/place/getall";
    public static final String INDEXSEARCH_FIND = "/indexsearch/find";
    public static final String SECRET = "Bakara Makara";
    public static final long EXPIRATION_TIME = 432_000_000; // 5 gün
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}

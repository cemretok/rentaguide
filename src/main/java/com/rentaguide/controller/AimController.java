package com.rentaguide.controller;

import com.rentaguide.entity.Aim;
import com.rentaguide.service.AimServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/aim")
public class AimController {
    private static final Logger log = LoggerFactory.getLogger(AimController.class);

    private AimServiceImpl aimService;

    public AimController(AimServiceImpl aimService) {
        this.aimService = aimService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Aim> saveAim(@RequestBody Aim aim) {
        log.info("saveAim function called");
        aimService.save(aim);
        return new ResponseEntity<Aim>(aim, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Aim> deleteAim(@RequestBody Aim aim) {
        log.info("deleteAim function called");
        aimService.delete(aim);
        return new ResponseEntity<Aim>(aim, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Aim> updateAim(@RequestBody Aim aim) {
        log.info("updateAim function called.");
        aimService.update(aim);
        return new ResponseEntity<Aim>(aim, HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Aim>> getAllAim() {
        log.info("getAllAim function called.");
        return new ResponseEntity<List<Aim>>(aimService.getAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Aim> getOneAim(@RequestBody Aim aim) {
        log.info("getOneAim function called.");
        return new ResponseEntity<Aim>(aimService.getOne(aim), HttpStatus.OK);
    }

}

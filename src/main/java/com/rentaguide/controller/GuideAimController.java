package com.rentaguide.controller;

import com.rentaguide.entity.GuideAim;
import com.rentaguide.service.GuideAimServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/guideaim")
public class GuideAimController {

    private static final Logger log = LoggerFactory.getLogger(GuideAimController.class);

    private GuideAimServiceImpl guideAimService;

    @Autowired
    public GuideAimController(GuideAimServiceImpl guideAimService) {
        this.guideAimService = guideAimService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideAim> saveGuideAim(@RequestBody GuideAim guideAim) {
        log.info("addGuideAim function called");
        guideAimService.save(guideAim);
        return new ResponseEntity<GuideAim>(guideAim, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideAim> deleteGuideAim(@RequestBody GuideAim guideAim) {
        log.info("deleteGuideAim function called");
        guideAimService.delete(guideAim);
        return new ResponseEntity<GuideAim>(guideAim, HttpStatus.OK);
    }

    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideAim> updateGuideAim(@RequestBody GuideAim guideAim) {
        log.info("updateGuideAim function called");
        guideAimService.update(guideAim);
        return new ResponseEntity<GuideAim>(guideAim, HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GuideAim>> getAllGuideAim() {
        log.info("getAllGuideAim function called");
        return new ResponseEntity<List<GuideAim>>(guideAimService.getAll(), HttpStatus.OK);
    }


    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideAim> getOne(@RequestBody GuideAim guideAim) {
        log.info("getOne function called.");
        return new ResponseEntity<GuideAim>(guideAimService.getOne(guideAim), HttpStatus.OK);
    }

}

package com.rentaguide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentaguide.entity.Guide;
import com.rentaguide.service.FileStorageService;
import com.rentaguide.service.GuideServiceImpl;

@CrossOrigin(origins = "", allowedHeaders = "")
@Controller
@RequestMapping("/guide")
public class GuideController {
    private static final Logger log = LoggerFactory.getLogger(GuideController.class);

    private GuideServiceImpl guideService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    @Autowired
	FileStorageService fileStorageService;
    ObjectMapper objectMapper = new ObjectMapper();

    public GuideController(GuideServiceImpl guideService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.guideService = guideService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Guide> deleteGuide(@RequestBody Guide guide) {
        log.info("deleteGuide function called.");
        guideService.delete(guide);
        return new ResponseEntity<Guide>(guide, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Guide> updateGuide(@RequestBody Guide guide) {
        log.info("updateGuide function called.");
        guideService.update(guide);
        return new ResponseEntity<Guide>(guide, HttpStatus.OK);
    }

    @PostMapping(value = "/find", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Guide>> findGuide(@RequestBody Guide guide) {
        log.info("findGuide function called.");
        return new
                ResponseEntity<List<Guide>>(guideService.find(guide),
                HttpStatus.OK);
    }
    
    
    @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Guide> signUp(@RequestBody Guide guide) {
        log.info("signUp function Crypted.");
        guide.setPassword(bCryptPasswordEncoder.encode(guide.getPassword()));
        guideService.save(guide);
        
        return new ResponseEntity<Guide>(guide, HttpStatus.OK);
    }
    /*
    
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Guide>  signUp(
    		@RequestParam("photo") MultipartFile file, 
    		@RequestParam("guide") String jsonData
    		) throws IOException {
    	Guide guide = objectMapper.readValue(jsonData, Guide.class);
        log.info("signUp function Crypted.");
        guide.setPassword(bCryptPasswordEncoder.encode(guide.getPassword()));
        try {
        	//guide.setProfilePicPath(file.getOriginalFilename().toString());
        	int guideId =guideService.save(guide);
        	System.out.println("file started");
        	System.out.println("guide_id "+guideId);
        	String guideName = "guide_"+guideId;
        	fileStorageService.store(file, guideName);
        	return new ResponseEntity<Guide>(guide, HttpStatus.OK);
        	
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
        return null;
    }
    */

    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Guide> getOne(@RequestBody Guide guide) {
        log.info("getOne function called.");
        //Resource file = fileStorageService.loadFile(guide.getProfilePicPath());
        return new ResponseEntity<Guide>(guideService.getOne(guide), HttpStatus.OK);
    }
}

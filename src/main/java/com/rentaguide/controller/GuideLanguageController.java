package com.rentaguide.controller;

import com.rentaguide.entity.GuideLanguage;
import com.rentaguide.service.GuideLanguageServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/guidelanguage")
public class GuideLanguageController {

    private static final Logger log = LoggerFactory.getLogger(GuideLanguageController.class);

    private GuideLanguageServiceImpl GuideLanguageService;

    @Autowired
    public GuideLanguageController(GuideLanguageServiceImpl GuideLanguageService) {
        this.GuideLanguageService = GuideLanguageService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideLanguage> saveGuideLanguage(@RequestBody GuideLanguage guideLanguage) {
        log.info("addGuideLanguage function called");
        GuideLanguageService.save(guideLanguage);
        return new ResponseEntity<GuideLanguage>(guideLanguage, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideLanguage> deleteGuideLanguage(@RequestBody GuideLanguage guideLanguage) {
        log.info("deleteGuideLanguage function called");
        GuideLanguageService.delete(guideLanguage);
        return new ResponseEntity<GuideLanguage>(guideLanguage, HttpStatus.OK);
    }

    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideLanguage> updateGuideLanguage(@RequestBody GuideLanguage guideLanguage) {
        log.info("updateGuideLanguage function called");
        GuideLanguageService.update(guideLanguage);
        return new ResponseEntity<GuideLanguage>(guideLanguage, HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GuideLanguage>> getAllGuideLanguage() {
        log.info("getAllGuideLanguage function called");
        return new ResponseEntity<List<GuideLanguage>>(GuideLanguageService.getAll(), HttpStatus.OK);
    }


    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuideLanguage> getOne(@RequestBody GuideLanguage guideLanguage) {
        log.info("getOne function called.");
        return new ResponseEntity<GuideLanguage>(GuideLanguageService.getOne(guideLanguage), HttpStatus.OK);
    }

}

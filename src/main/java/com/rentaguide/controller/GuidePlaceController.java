package com.rentaguide.controller;

import com.rentaguide.entity.GuidePlace;
import com.rentaguide.service.GuidePlaceServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/guideplace")
public class GuidePlaceController {

    private static final Logger log = LoggerFactory.getLogger(GuidePlaceController.class);

    private GuidePlaceServiceImpl guidePlaceService;

    @Autowired
    public GuidePlaceController(GuidePlaceServiceImpl guidePlaceService) {
        this.guidePlaceService = guidePlaceService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePlace> saveGuidePlace(@RequestBody GuidePlace guidePlace) {
        log.info("addGuidePlace function called");
        guidePlaceService.save(guidePlace);
        return new ResponseEntity<GuidePlace>(guidePlace, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePlace> deleteGuidePlace(@RequestBody GuidePlace guidePlace) {
        log.info("deleteGuidePlace function called");
        guidePlaceService.delete(guidePlace);
        return new ResponseEntity<GuidePlace>(guidePlace, HttpStatus.OK);
    }

    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePlace> updateGuidePlace(@RequestBody GuidePlace guidePlace) {
        log.info("updateGuidePlace function called");
        guidePlaceService.update(guidePlace);
        return new ResponseEntity<GuidePlace>(guidePlace, HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GuidePlace>> getAllGuidePlace() {
        log.info("getAllGuidePlace function called");
        return new ResponseEntity<List<GuidePlace>>(guidePlaceService.getAll(), HttpStatus.OK);
    }


    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePlace> getOne(@RequestBody GuidePlace guidePlace) {
        log.info("getOne function called.");
        return new ResponseEntity<GuidePlace>(guidePlaceService.getOne(guidePlace), HttpStatus.OK);
    }

}

package com.rentaguide.controller;

import com.rentaguide.entity.GuidePrice;
import com.rentaguide.service.GuidePriceServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/guideprice")
public class GuidePriceController {

    private static final Logger log = LoggerFactory.getLogger(GuidePriceController.class);

    private GuidePriceServiceImpl guidePriceService;

    @Autowired
    public GuidePriceController(GuidePriceServiceImpl guidePriceService) {
        this.guidePriceService = guidePriceService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePrice> saveGuidePrice(@RequestBody GuidePrice guidePrice) {
        log.info("addGuidePrice function called");
        guidePriceService.save(guidePrice);
        return new ResponseEntity<GuidePrice>(guidePrice, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePrice> deleteGuidePrice(@RequestBody GuidePrice guidePrice) {
        log.info("deleteGuidePrice function called");
        guidePriceService.delete(guidePrice);
        return new ResponseEntity<GuidePrice>(guidePrice, HttpStatus.OK);
    }

    @GetMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePrice> updateGuidePrice(@RequestBody GuidePrice guidePrice) {
        log.info("updateGuidePrice function called");
        guidePriceService.update(guidePrice);
        return new ResponseEntity<GuidePrice>(guidePrice, HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<GuidePrice>> getAllGuidePrice() {
        log.info("getAllGuidePrice function called");
        return new ResponseEntity<List<GuidePrice>>(guidePriceService.getAll(), HttpStatus.OK);
    }


    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuidePrice> getOne(@RequestBody GuidePrice guidePrice) {
        log.info("getOne function called.");
        return new ResponseEntity<GuidePrice>(guidePriceService.getOne(guidePrice), HttpStatus.OK);
    }

}

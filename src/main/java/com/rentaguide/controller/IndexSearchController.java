package com.rentaguide.controller;

import java.util.List;

import com.rentaguide.entity.IndexSearch;
import com.rentaguide.service.IndexSearchServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "", allowedHeaders = "")
@Controller
@RequestMapping("/indexsearch")
public class IndexSearchController {
    private static final Logger log = LoggerFactory.getLogger(IndexSearchController.class);

    private IndexSearchServiceImpl indexSearchService;

    @Autowired
    public IndexSearchController(IndexSearchServiceImpl indexSearchService) {
        this.indexSearchService = indexSearchService;
    }

    @PostMapping(value = "/find", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<IndexSearch>> find(@RequestBody IndexSearch indexSearch) {
        log.info("find function called.");
        // indexSearchService.save(indexSearch);
        return new ResponseEntity<List<IndexSearch>>(indexSearchService.find(indexSearch), HttpStatus.OK);
    }

    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IndexSearch> getOne(@RequestBody IndexSearch indexSearch) {
        log.info("getOne function called.");
        indexSearchService.getOne(indexSearch);
        return new ResponseEntity<IndexSearch>(indexSearchService.getOne(indexSearch), HttpStatus.OK);
    }

}

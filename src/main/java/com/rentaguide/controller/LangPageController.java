package com.rentaguide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rentaguide.entity.LanguagePage;
import com.rentaguide.service.LangPageServiceImpl;


@CrossOrigin(origins = "")
@Controller
@RequestMapping("/languagepage")
public class LangPageController {
	private static final Logger log = LoggerFactory.getLogger(LangPageController.class);

    private LangPageServiceImpl languageService;

    @Autowired
    public LangPageController(LangPageServiceImpl languageService) {
		this.languageService = languageService;
	}

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LanguagePage> saveLangPage(@RequestBody LanguagePage language) {
        log.info("addLanguagePage function called.");
        languageService.save(language);
        return new ResponseEntity<LanguagePage>(language, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LanguagePage> deleteLangPage(@RequestBody LanguagePage language) {
        log.info("deleteLanguagePage function called.");
        languageService.delete(language);
        return new ResponseEntity<LanguagePage>(language, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LanguagePage> updateLangPage(@RequestBody LanguagePage language) {
        log.info("updateLanguagePage function called.");
        languageService.update(language);
        return new ResponseEntity<LanguagePage>(language, HttpStatus.OK);
    }

    @GetMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LanguagePage> getOneLangPage(@RequestBody LanguagePage language) {
        log.info("getOneLanguagePage function called.");
        return new ResponseEntity<LanguagePage>(languageService.getOne(language), HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<LanguagePage>> getAllLangPage() {
        log.info("getAllLanguagePage function called.");
        return new ResponseEntity<List<LanguagePage>>(languageService.getAll(), HttpStatus.OK);
    }

}

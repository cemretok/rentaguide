package com.rentaguide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rentaguide.entity.Language;
import com.rentaguide.service.LanguageServiceImpl;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/language")
public class LanguageController {
    private static final Logger log = LoggerFactory.getLogger(LanguageController.class);

    private LanguageServiceImpl languageService;

    @Autowired
    public LanguageController(LanguageServiceImpl languageService) {
        this.languageService = languageService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Language> saveLanguage(@RequestBody Language language) {
        log.info("addLanguage function called.");
        languageService.save(language);
        return new ResponseEntity<Language>(language, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Language> deleteLanguage(@RequestBody Language language) {
        log.info("deleteLanguage function called.");
        languageService.delete(language);
        return new ResponseEntity<Language>(language, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Language> updateLanguage(@RequestBody Language language) {
        log.info("updateLanguage function called.");
        languageService.update(language);
        return new ResponseEntity<Language>(language, HttpStatus.OK);
    }

    @GetMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Language> getOneLanguage(@RequestBody Language language) {
        log.info("getOneLanguage function called.");
        return new ResponseEntity<Language>(languageService.getOne(language), HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Language>> getAllLanguage() {
        log.info("getAllLanguage function called.");
        return new ResponseEntity<List<Language>>(languageService.getAll(), HttpStatus.OK);
    }

}

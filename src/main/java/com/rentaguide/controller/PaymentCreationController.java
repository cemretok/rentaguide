package com.rentaguide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rentaguide.entity.PaymentCreation;
import com.rentaguide.entity.PaymentResponse;
import com.rentaguide.service.PaymentCreationServiceImpl;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/makepayment")
public class PaymentCreationController {
	private static final Logger log = LoggerFactory.getLogger(PaymentCreationController.class);
	
	private PaymentCreationServiceImpl paymentCreationService;
	
	public PaymentCreationController (PaymentCreationServiceImpl paymentCreationService) {
		this.paymentCreationService = paymentCreationService;
	}
	
	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PaymentCreation> savePaymentCreation(@RequestBody PaymentCreation payCreation) {
        log.info("savePaymentCreation function called");
        paymentCreationService.save(payCreation);
        return new ResponseEntity<PaymentCreation>(payCreation, HttpStatus.OK);
    }
	
	@DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentCreation> deletePaymentCreation(@RequestBody PaymentCreation payCreation) {
        log.info("deletePaymentCreation function called");
        paymentCreationService.delete(payCreation);
        return new ResponseEntity<PaymentCreation>(payCreation, HttpStatus.OK);
    }
	
	@PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentCreation> updatePaymentCreation(@RequestBody PaymentCreation payCreation) {
        log.info("updatePaymentCreation function called.");
        paymentCreationService.update(payCreation);
        return new ResponseEntity<PaymentCreation>(payCreation, HttpStatus.OK);
    }
	
    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PaymentCreation>> getAllPaymentCreation() {
        log.info("getAllPaymentCreation function called.");
        return new ResponseEntity<List<PaymentCreation>>(paymentCreationService.getAll(), HttpStatus.OK);
    }

    @GetMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentCreation> getOnePaymentCreation(@RequestBody PaymentCreation payCreation) {
        log.info("getPaymentCreation function called.");
        return new ResponseEntity<PaymentCreation>(paymentCreationService.getOne(payCreation), HttpStatus.OK);
    }
    
    @PostMapping(value = "/makePayment", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentResponse> makePaymentCreation(@RequestBody PaymentCreation payCreation){
		log.info("makePaymentCreation function called");
    	return new ResponseEntity<PaymentResponse>(paymentCreationService.makePayment(payCreation), HttpStatus.OK);
		//paymentCreationService.makePayment(payCreation);
    }
	
}

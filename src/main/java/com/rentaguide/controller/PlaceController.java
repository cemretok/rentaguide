package com.rentaguide.controller;

import com.rentaguide.entity.Place;
import com.rentaguide.service.PlaceServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "")
@Controller
@RequestMapping("/place")
public class PlaceController {
    private static final Logger log = LoggerFactory.getLogger(PlaceController.class);

    private PlaceServiceImpl placeService;

    @Autowired
    public PlaceController(PlaceServiceImpl placeService) {
        this.placeService = placeService;
    }

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Place> savePlace(@RequestBody Place place) {
        log.info("savePlace function called.");
        placeService.save(place);
        return new ResponseEntity<Place>(place, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Place> deletePlace(@RequestBody Place place) {
        log.info("deletePlace function called.");
        placeService.delete(place);
        return new ResponseEntity<Place>(place, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Place> updatePlace(@RequestBody Place place) {
        log.info("updatePlace function called.");
        placeService.update(place);
        return new ResponseEntity<Place>(place, HttpStatus.OK);
    }

    @PostMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Place> getOnePlace(@RequestBody Place place) {
        log.info("getOnePlace function called.");
        return new ResponseEntity<Place>(placeService.getOne(place), HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Place>> getAllPlace() {
        log.info("getAllPlace function called.");
        return new ResponseEntity<List<Place>>(placeService.getAll(), HttpStatus.OK);
    }

}

package com.rentaguide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rentaguide.entity.Guide;
import com.rentaguide.entity.Reservation;
import com.rentaguide.entity.User;
import com.rentaguide.service.ReservationServiceImpl;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/reservation")
public class ReservationController {
    private static final Logger log = LoggerFactory.getLogger(ReservationController.class);

    private ReservationServiceImpl reservationService;

    @Autowired
    public ReservationController(ReservationServiceImpl reservationService) {
        this.reservationService = reservationService;
    }
    

    @PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Reservation> saveReservation(@RequestBody Reservation reservation) {
        log.info("saveReservation function called.");
        
        try {
            reservationService.save(reservation);
            reservation.setReservationStatus("Success");
        } catch (Exception e) {
            reservation.setReservationStatus("Fail");
        }
        return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Reservation> deleteReservation(@RequestBody Reservation reservation) {
        log.info("deleteReservation function called.");
        reservationService.delete(reservation);
        return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Reservation> updateReservation(@RequestBody Reservation reservation) {
        log.info("updateReservation function called.");
        reservationService.update(reservation);
        return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
    }

    @GetMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Reservation> getOne(@RequestBody Reservation reservation) {
        log.info("getOne for ReservationId function called.");
        return new ResponseEntity<Reservation>(reservationService.getOne(reservation), HttpStatus.OK);
    }

    @GetMapping(value = "/getallres", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reservation>> getAllRes() {
        log.info("getallresid function called for all reservations.");
        return new ResponseEntity<List<Reservation>>(reservationService.getAll(), HttpStatus.OK);
    }
    
    @GetMapping(value = "/getalluser", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reservation>> getAllOneUser(@RequestBody User user) {
        log.info("getalluser reservations function called for one user.");
        return new ResponseEntity<List<Reservation>>(reservationService.getAllUsers(user.getId()), HttpStatus.OK);
    }
    
    @GetMapping(value = "/getallguide", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reservation>> getAllOneGuide(@RequestBody Guide guide) {
        log.info("getallguideid reservations function called for one guide.");
        return new ResponseEntity<List<Reservation>>(reservationService.getAllGuides(guide.getId()), HttpStatus.OK);
    }

}

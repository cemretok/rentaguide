package com.rentaguide.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rentaguide.entity.Timesheet;
import com.rentaguide.service.TimesheetServiceImpl;

@CrossOrigin(origins = "")
@Controller
@RequestMapping("/timesheet")
public class TimesheetController {
	private static final Logger log = LoggerFactory.getLogger(TimesheetController.class);
	
	private TimesheetServiceImpl timesheetService;
	
	@Autowired
	public TimesheetController (TimesheetServiceImpl timesheetService) {
		this.timesheetService = timesheetService;
	}
	
	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Timesheet> saveTimesheet(@RequestBody Timesheet timesheet) {
        log.info("saveTimesheet function called.");
        timesheetService.save(timesheet);
        return new ResponseEntity<Timesheet>(timesheet, HttpStatus.OK);
    }
	
    @DeleteMapping(value = "/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Timesheet> deleteTimesheet(@RequestBody Timesheet timesheet) {
        log.info("deleteTimesheet function called.");
        timesheetService.delete(timesheet);
        return new ResponseEntity<Timesheet>(timesheet, HttpStatus.OK);
    }

    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Timesheet> updateTimesheet(@RequestBody Timesheet timesheet) {
        log.info("updateTimesheet function called.");
        timesheetService.update(timesheet);
        return new ResponseEntity<Timesheet>(timesheet, HttpStatus.OK);
    }

    @GetMapping(value = "/getone", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Timesheet> getOne(@RequestBody Timesheet timesheet) {
        log.info("getOne function called.");
        return new ResponseEntity<Timesheet>(timesheetService.getOne(timesheet), HttpStatus.OK);
    }

    @GetMapping(value = "/getall", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Timesheet>> getAll() {
        log.info("getAll function called.");
        return new ResponseEntity<List<Timesheet>>(timesheetService.getAll(), HttpStatus.OK);
    }
    

}

package com.rentaguide.entity;

import java.io.Serializable; 

public class Aim implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1575197606009467865L;

    private Integer id;
    private String aim;
    private String aimDetail; 

    public Aim() {
        super();
    }

    public Aim(Integer id) {
        this.id = id;
    }

    public Aim(String aim, String aimDetail) {
        super();
        this.aim = aim;
        this.aimDetail = aimDetail;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public String getAimDetail() {
        return aimDetail;
    }

    public void setAimDetail(String aimDetail) {
        this.aimDetail = aimDetail;
    }

    @Override
    public String toString() {
        return "Aim [id=" + id + ", aim=" + aim + ", aimDetail=" + aimDetail + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aim == null) ? 0 : aim.hashCode());
        result = prime * result + ((aimDetail == null) ? 0 : aimDetail.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Aim other = (Aim) obj;
        if (aim == null) {
            if (other.aim != null)
                return false;
        } else if (!aim.equals(other.aim))
            return false;
        if (aimDetail == null) {
            if (other.aimDetail != null)
                return false;
        } else if (!aimDetail.equals(other.aimDetail))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

}

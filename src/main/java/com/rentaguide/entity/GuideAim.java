package com.rentaguide.entity;

import java.io.Serializable;

public class GuideAim implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1904826418308197359L;

    private Guide guide;
    private Aim aim;

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }

    public Aim getAim() {
        return aim;
    }

    public void setAim(Aim aim) {
        this.aim = aim;
    }
}

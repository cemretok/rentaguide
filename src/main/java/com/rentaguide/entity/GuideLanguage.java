package com.rentaguide.entity;

import java.io.Serializable;

public class GuideLanguage implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1904826418308197359L;

    private Guide guide;
    private Language language;

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}

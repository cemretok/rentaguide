package com.rentaguide.entity;

import java.io.Serializable;

public class GuidePlace implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1904826418308197359L;

    private Guide guide;
    private Place place;

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}

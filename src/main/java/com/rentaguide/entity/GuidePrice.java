package com.rentaguide.entity;

import java.io.Serializable;

public class GuidePrice implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1904826418308197359L;

    private Integer id;
    private Place place;
    private Guide guide;
    private Integer time;
    private Integer guidingPrice;
    private Integer drivingPrice;
    private String currency;

    public GuidePrice() {
        super();
    }

    public GuidePrice(Place place, Guide guide) {
        this.place = place;
        this.guide = guide;
    }

    public GuidePrice(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public Integer getGuidingPrice() {
        return guidingPrice;
    }

    public void setGuidingPrice(Integer guidingPrice) {
        this.guidingPrice = guidingPrice;
    }

    public Integer getDrivingPrice() {
        return drivingPrice;
    }

    public void setDrivingPrice(Integer drivingPrice) {
        this.drivingPrice = drivingPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}

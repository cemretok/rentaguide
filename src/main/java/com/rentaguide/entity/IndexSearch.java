package com.rentaguide.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public class IndexSearch implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1009077311112437913L;
    /**
     *
     */

    private Integer id;
    private Place place;
	private Aim aim;
    private Guide guide;
    private GuidePrice guidePrice;
    private User user;
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate checkin;
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate checkout;
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate searchdate;
    private String gender;
    private String driverLicence;
    private Double minPrice;
    private Double maxPrice;
    private String currency;
    private Integer getMyDriver;
    private List<Language> lang;
    
    

	public List<Language> getLang() {
		return lang;
	}

	public void setLang(List<Language> lang) {
		this.lang = lang;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Aim getAim() {
        return aim;
    }

    public void setAim(Aim aim) {
        this.aim = aim;
    }

    public LocalDate getCheckin() {
        return checkin;
    }

    public void setCheckin(LocalDate checkin) {
        this.checkin = checkin;
    }

    public Guide getGuide() {
        return guide;
    }

    public void setGuide(Guide guide) {
        this.guide = guide;
    }

    public GuidePrice getGuidePrice() {
        return guidePrice;
    }

    public void setGuidePrice(GuidePrice guidePrice) {
        this.guidePrice = guidePrice;
    }

    public LocalDate getCheckout() {
        return checkout;
    }

    public void setCheckout(LocalDate checkout) {
        this.checkout = checkout;
    }

    public LocalDate getSearchdate() {
        return searchdate;
    }

    public void setSearchdate(LocalDate searchdate) {
        this.searchdate = searchdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDriverLicence() {
        return driverLicence;
    }

    public void setDriverLicence(String driverLicence) {
        this.driverLicence = driverLicence;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
    
    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	 

    public Integer getGetMyDriver() {
		return getMyDriver;
	}

	public void setGetMyDriver(Integer getMyDriver) {
		this.getMyDriver = getMyDriver;
	}

	
    @Override
    public String toString() {
        return "IndexSearch{" +
                "id=" + id +
                ", place=" + place +
                ", aim=" + aim +
                ", guide=" + guide +
                ", guidePrice=" + guidePrice +
                ", checkin=" + checkin +
                ", checkout=" + checkout +
                ", searchdate=" + searchdate +
                ", gender='" + gender + '\'' +
                ", driverLicence='" + driverLicence + '\'' +
                ", minPrice=" + minPrice +
                ", maxPrice=" + maxPrice +
                ", currency='" + currency + '\'' +
                '}';
    }
}

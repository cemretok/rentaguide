package com.rentaguide.entity;

import java.io.Serializable;

public class Language implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5644497392462624084L;

    private Integer id;
    private String name;

    public Language() {
        super();
    }

    public Language(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Language [id=" + id + ", name=" + name + "]";
    }

}

package com.rentaguide.entity;

import java.io.Serializable;

public class LanguagePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6965809897860135165L;
	
	private Integer languageId;
	
	private String languageName;
	
	private String languageDesc;

	public LanguagePage() {
		super();
	}

	public LanguagePage(Integer languageId, String languageName, String languageDesc) {
		super();
		this.languageId = languageId;
		this.languageName = languageName;
		this.languageDesc = languageDesc;
	}

	public Integer getLanguageId() {
		return languageId;
	}

	public void setLanguageId(Integer languageId) {
		this.languageId = languageId;
	}

	public String getLanguageName() {
		return languageName;
	}

	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	public String getLanguageDesc() {
		return languageDesc;
	}

	public void setLanguageDesc(String languageDesc) {
		this.languageDesc = languageDesc;
	}

	@Override
	public String toString() {
		return "LanguagePage [languageId=" + languageId + ", languageName=" + languageName + ", languageDesc="
				+ languageDesc + "]";
	}
	
	

}

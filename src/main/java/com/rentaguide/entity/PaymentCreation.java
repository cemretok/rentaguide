package com.rentaguide.entity;

import java.io.Serializable;

public class PaymentCreation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1374894957940394700L;

	
	private Integer id;
	private int reservationId;
	private String cardHolder;
	private String cardNumber;
	private String expirationMonth;
	private String expirationYear;
	private String cvcNumber;
	
	public PaymentCreation() {
		super();
	}
	
	public PaymentCreation(int reservationId, String cardHolder, String cardNumber, String expirationMonth,
			String expirationYear, String cvcNumber, int reservation) {
		super();
		this.reservationId = reservationId;
		this.cardHolder = cardHolder;
		this.cardNumber = cardNumber;
		this.expirationMonth = expirationMonth;
		this.expirationYear = expirationYear;
		this.cvcNumber = cvcNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getReservationId() {
		return reservationId;
	}
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
	public String getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpirationMonth() {
		return expirationMonth;
	}
	public void setExpirationMonth(String expirationMonth) {
		this.expirationMonth = expirationMonth;
	}
	public String getExpirationYear() {
		return expirationYear;
	}
	public void setExpirationYear(String expirationYear) {
		this.expirationYear = expirationYear;
	}
	public String getCvcNumber() {
		return cvcNumber;
	}
	public void setCvcNumber(String cvcNumber) {
		this.cvcNumber = cvcNumber;
	}

	@Override
	public String toString() {
		return "PaymentCreation [id=" + id + ", reservationId=" + reservationId + ", cardHolder=" + cardHolder
				+ ", cardNumber=" + cardNumber + ", expirationMonth=" + expirationMonth + ", expirationYear="
				+ expirationYear + ", cvcNumber=" + cvcNumber + "]";
	}
	

}

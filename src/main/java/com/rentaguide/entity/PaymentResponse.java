package com.rentaguide.entity;

import java.io.Serializable;

public class PaymentResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5625515858440385629L;
	
	private Integer id;
	private String status;
	private String description;
	
	
	public PaymentResponse() {
		super();
	}
	
	public PaymentResponse(Integer id, String status, String description) {
		super();
		this.id = id;
		this.status = status;
		this.description = description;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return "PaymentResponse [id=" + id + ", status=" + status + ", description=" + description + "]";
	}
	
	

}

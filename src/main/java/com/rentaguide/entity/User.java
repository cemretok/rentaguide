package com.rentaguide.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;

public class User implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1346501219995434528L;

	private Integer id;
	private String name;
	private String surname;
	private String password;
	private String mail;
	private String phone;

	@JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime registerDate;
	private String passportNo;

	public User() {
		super();
	}

	public User(Integer id, String name, String surname, String password, String mail, String phone,
			LocalDateTime registerDate, String passportNo) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.password = password;
		this.mail = mail;
		this.phone = phone;
		this.registerDate = registerDate;
		this.passportNo = passportNo;
	}

	public User(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(LocalDateTime registerDate) {
		this.registerDate = registerDate;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", name='" + name + '\'' + ", surname='" + surname + '\'' + ", password='"
				+ password + '\'' + ", mail='" + mail + '\'' + ", phone='" + phone + '\'' + ", registerDate='"
				+ registerDate + '\'' + ", passportNo='" + passportNo + '\'' + '}';
	}
}

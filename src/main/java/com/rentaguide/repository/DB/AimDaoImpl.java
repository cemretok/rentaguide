package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Aim;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.AimSchemaSql;

@Repository
public class AimDaoImpl implements SimpleDao<Aim> {

    public HKDataSource dataSource;

    public AimDaoImpl(HKDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Aim> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(AimSchemaSql.SQL_AIM_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<Aim> aimList = new ArrayList<Aim>();
            while (rs.next()) {
                Aim a = new Aim();

                a.setId(rs.getInt("id"));
                a.setAim(rs.getString("aim"));
                a.setAimDetail(rs.getString("aim_detail"));

                aimList.add(a);
            }

            return aimList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Aim getOne(Aim aim) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(AimSchemaSql.SQL_AIM_GET_ONE)) {
            preparedStatement.setInt(1, aim.getId());
            ResultSet rs = preparedStatement.executeQuery();
            Aim a = new Aim();
            while (rs.next()) {

                a.setId(rs.getInt("id"));
                a.setAim(rs.getString("aim"));
                a.setAimDetail(rs.getString("aim_detail"));

            }

            return a;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int save(Aim t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(AimSchemaSql.SQL_AIM_SAVE)) {
            preparedStatement.setString(1, t.getAim());
            preparedStatement.setString(2, t.getAimDetail());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(Aim t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(AimSchemaSql.SQL_AIM_UPDATE)) {
            preparedStatement.setString(1, t.getAim());
            preparedStatement.setString(2, t.getAimDetail());
            preparedStatement.setInt(3, t.getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(Aim t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(AimSchemaSql.SQL_AIM_DELETE)) {
            preparedStatement.setInt(1, t.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Aim;
import com.rentaguide.entity.Guide;
import com.rentaguide.entity.GuideAim;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.GuideAimSchemaSql;

@Repository
public class GuideAimDaoImpl implements SimpleDao<GuideAim> {

    public HKDataSource dataSource;
    public GuideDaoImpl guideDao;
    public AimDaoImpl aimDao;

    public GuideAimDaoImpl(HKDataSource dataSource, GuideDaoImpl guideDao, AimDaoImpl aimDao) {
        this.dataSource = dataSource;
        this.guideDao = guideDao;
        this.aimDao = aimDao;
    }

    @Override
    public List<GuideAim> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuideAimSchemaSql.SQL_GUIDE_AIM_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<GuideAim> guideAimList = new ArrayList<GuideAim>();
            while (rs.next()) {
                GuideAim ga = new GuideAim();

                ga.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                ga.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));

                guideAimList.add(ga);
            }

            return guideAimList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public GuideAim getOne(GuideAim guideAim) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuideAimSchemaSql.SQL_GUIDE_AIM_GET_ONE)) {
            preparedStatement.setInt(1, guideAim.getGuide().getId());
            preparedStatement.setInt(2, guideAim.getAim().getId());
            ResultSet rs = preparedStatement.executeQuery();

            GuideAim gp = new GuideAim();
            while (rs.next()) {
                gp.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gp.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));
            }

            return gp;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public int save(GuideAim guideAim) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideAimSchemaSql.SQL_GUIDE_AIM_SAVE)) {
            preparedStatement.setInt(1, guideAim.getGuide().getId());
            preparedStatement.setInt(2, guideAim.getAim().getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(GuideAim guideAim) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideAimSchemaSql.SQL_GUIDE_AIM_UPDATE)) {
            preparedStatement.setInt(1, guideAim.getGuide().getId());
            preparedStatement.setInt(2, guideAim.getAim().getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(GuideAim guideAim) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideAimSchemaSql.SQL_GUIDE_AIM_DELETE)) {
            preparedStatement.setInt(1, guideAim.getGuide().getId());
            preparedStatement.setInt(1, guideAim.getAim().getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

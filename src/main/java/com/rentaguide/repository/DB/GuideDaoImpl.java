package com.rentaguide.repository.DB;

import com.rentaguide.entity.Guide;
import com.rentaguide.repository.DB.Interfaces.UserDao;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.SQL.GuideSchemaSql;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GuideDaoImpl implements UserDao<Guide> {

    public HKDataSource dataSource;

    public GuideDaoImpl(HKDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Guide> find(Guide t) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuideSchemaSql.SQL_GUIDE_FIND)) {
            System.out.println("t:" + t.toString());
            preparedStatement.setObject(1, t.getId(), Types.INTEGER);
            preparedStatement.setObject(2, t.getMail(), Types.VARCHAR);

            ResultSet rs = preparedStatement.executeQuery();
            List<Guide> userList = new ArrayList<Guide>();
            while (rs.next()) {
                Guide u = new Guide();
                u.setId(rs.getInt("id"));
                u.setMail(rs.getString("mail"));
                u.setName(rs.getString("name"));
                u.setSurname(rs.getString("surname"));
                u.setPassword(rs.getString("password"));
                u.setPhone(rs.getString("phone"));
                u.setRegisterDate(rs.getObject("register_date", LocalDateTime.class));
                u.setProfile(rs.getString("profile"));
                u.setProfilePicPath(rs.getString("picture"));
                u.setGender(rs.getString("gender"));

                userList.add(u);
            }

            return userList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Guide getOne(Guide guide) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuideSchemaSql.SQL_GUIDE_GET_ONE)) {
            preparedStatement.setInt(1, guide.getId());
            ResultSet rs = preparedStatement.executeQuery();

            Guide g = new Guide();
            while (rs.next()) {

                g.setId(rs.getInt("id"));
                g.setDriverLicence(rs.getString("driver_licence"));
                g.setGender(rs.getString("gender"));
                g.setMail(rs.getString("mail"));
                g.setName(rs.getString("name"));
                g.setPhone(rs.getString("phone"));
                g.setProfilePicPath(rs.getString("picture"));
                g.setProfile(rs.getString("profile"));
                g.setRegisterDate(rs.getObject("register_date", LocalDateTime.class));
                g.setSurname(rs.getString("surname"));
               // g.setProfilePicPath(rs.getString("profilePicPath"));


            }

            return g;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Guide findByMail(String mail) {
        Guide g = new Guide();
        g.setMail(mail);
        List<Guide> guideList = find(g);

        if (guideList.size() > 0) {
            return guideList.get(0);
        }
        else if(guideList.size() == 0){
            return null;
        }

        return null;
    }

    @Override
    public int save(Guide t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideSchemaSql.SQL_GUIDE_SAVE)) {
            preparedStatement.setString(1, t.getDriverLicence());
            preparedStatement.setString(2, t.getGender());
            preparedStatement.setString(3, t.getMail());
            preparedStatement.setString(4, t.getName());
            preparedStatement.setString(5, t.getPassword());
            preparedStatement.setString(6, t.getPhone());
            preparedStatement.setString(7, t.getProfilePicPath());
            preparedStatement.setString(8, t.getProfile());
            preparedStatement.setObject(9, t.getRegisterDate());
            preparedStatement.setString(10, t.getSurname());
           // preparedStatement.setString(11, t.getProfilePicPath());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(Guide t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideSchemaSql.SQL_GUIDE_UPDATE)) {

            preparedStatement.setString(1, t.getName());
            preparedStatement.setString(2, t.getSurname());
            preparedStatement.setString(3, t.getPhone());
            preparedStatement.setString(4, t.getProfilePicPath());
            preparedStatement.setString(5, t.getProfile());
            preparedStatement.setInt(6, t.getId());
            preparedStatement.setString(7, t.getProfilePicPath());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(Guide t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideSchemaSql.SQL_GUIDE_DELETE)) {
            preparedStatement.setInt(1, t.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

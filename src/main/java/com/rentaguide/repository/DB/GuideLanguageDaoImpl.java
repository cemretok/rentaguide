package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Guide;
import com.rentaguide.entity.GuideLanguage;
import com.rentaguide.entity.Language;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.GuideLanguageSchemaSql;

@Repository
public class GuideLanguageDaoImpl implements SimpleDao<GuideLanguage> {

    public HKDataSource dataSource;
    public GuideDaoImpl guideDao;
    public LanguageDaoImpl languageDao;

    public GuideLanguageDaoImpl(HKDataSource dataSource, GuideDaoImpl guideDao, LanguageDaoImpl languageDao) {
        this.dataSource = dataSource;
        this.guideDao = guideDao;
        this.languageDao = languageDao;
    }

    @Override
    public List<GuideLanguage> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuideLanguageSchemaSql.SQL_GUIDE_LANGUAGE_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<GuideLanguage> guideLanguageList = new ArrayList<GuideLanguage>();
            while (rs.next()) {
                GuideLanguage gl = new GuideLanguage();

                gl.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gl.setLanguage(languageDao.getOne(new Language(rs.getInt("language_id"))));

                guideLanguageList.add(gl);
            }

            return guideLanguageList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public GuideLanguage getOne(GuideLanguage guideLanguage) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuideLanguageSchemaSql.SQL_GUIDE_LANGUAGE_GET_ONE)) {
            preparedStatement.setInt(1, guideLanguage.getGuide().getId());
            preparedStatement.setInt(2, guideLanguage.getLanguage().getId());
            ResultSet rs = preparedStatement.executeQuery();

            GuideLanguage gp = new GuideLanguage();
            while (rs.next()) {
                gp.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gp.setLanguage(languageDao.getOne(new Language(rs.getInt("language_id"))));
            }

            return gp;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public int save(GuideLanguage guideLanguage) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideLanguageSchemaSql.SQL_GUIDE_LANGUAGE_SAVE)) {
            preparedStatement.setInt(1, guideLanguage.getGuide().getId());
            preparedStatement.setInt(2, guideLanguage.getLanguage().getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(GuideLanguage guideLanguage) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideLanguageSchemaSql.SQL_GUIDE_LANGUAGE_UPDATE)) {
            preparedStatement.setInt(1, guideLanguage.getGuide().getId());
            preparedStatement.setInt(2, guideLanguage.getLanguage().getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(GuideLanguage guideLanguage) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuideLanguageSchemaSql.SQL_GUIDE_LANGUAGE_DELETE)) {
            preparedStatement.setInt(1, guideLanguage.getGuide().getId());
            preparedStatement.setInt(1, guideLanguage.getLanguage().getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Guide;
import com.rentaguide.entity.GuidePlace;
import com.rentaguide.entity.Place;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.GuidePlaceSchemaSql;

@Repository
public class GuidePlaceDaoImpl implements SimpleDao<GuidePlace> {

    public HKDataSource dataSource;
    public GuideDaoImpl guideDao;
    public PlaceDaoImpl placeDao;

    public GuidePlaceDaoImpl(HKDataSource dataSource, GuideDaoImpl guideDao, PlaceDaoImpl placeDao) {
        this.dataSource = dataSource;
        this.guideDao = guideDao;
        this.placeDao = placeDao;
    }

    @Override
    public List<GuidePlace> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuidePlaceSchemaSql.SQL_GUIDE_PLACE_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<GuidePlace> GuidePlaceList = new ArrayList<GuidePlace>();
            while (rs.next()) {
                GuidePlace gp = new GuidePlace();

                gp.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gp.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));

                GuidePlaceList.add(gp);
            }

            return GuidePlaceList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public GuidePlace getOne(GuidePlace guidePlace) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuidePlaceSchemaSql.SQL_GUIDE_PLACE_GET_ONE)) {
            preparedStatement.setInt(1, guidePlace.getGuide().getId());
            preparedStatement.setInt(2, guidePlace.getPlace().getId());
            ResultSet rs = preparedStatement.executeQuery();

            GuidePlace gp = new GuidePlace();
            while (rs.next()) {
                gp.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gp.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
            }

            return gp;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public int save(GuidePlace guidePlace) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuidePlaceSchemaSql.SQL_GUIDE_PLACE_SAVE)) {
            preparedStatement.setInt(1, guidePlace.getGuide().getId());
            preparedStatement.setInt(2, guidePlace.getPlace().getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(GuidePlace guidePlace) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuidePlaceSchemaSql.SQL_GUIDE_PLACE_UPDATE)) {
            preparedStatement.setInt(1, guidePlace.getGuide().getId());
            preparedStatement.setInt(2, guidePlace.getPlace().getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(GuidePlace guidePlace) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuidePlaceSchemaSql.SQL_GUIDE_PLACE_DELETE)) {
            preparedStatement.setInt(1, guidePlace.getGuide().getId());
            preparedStatement.setInt(1, guidePlace.getPlace().getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

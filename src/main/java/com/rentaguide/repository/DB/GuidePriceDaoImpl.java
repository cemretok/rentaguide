package com.rentaguide.repository.DB;

import com.rentaguide.entity.Guide;
import com.rentaguide.entity.GuidePrice;
import com.rentaguide.entity.IndexSearch;
import com.rentaguide.entity.Place;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.SQL.GuidePriceSchemaSql;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException; 
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GuidePriceDaoImpl implements SimpleDao<GuidePrice> {

    public HKDataSource dataSource;
    public GuideDaoImpl guideDao;
    public PlaceDaoImpl placeDao;

    public GuidePriceDaoImpl(HKDataSource dataSource, GuideDaoImpl guideDao, PlaceDaoImpl placeDao) {
        this.dataSource = dataSource;
        this.guideDao = guideDao;
        this.placeDao = placeDao;
    }

    @Override
    public List<GuidePrice> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuidePriceSchemaSql.SQL_GUIDE_PRICE_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<GuidePrice> guidePriceList = new ArrayList<GuidePrice>();
            while (rs.next()) {
                GuidePrice gp = new GuidePrice();

                gp.setId(rs.getInt("id"));
                gp.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gp.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
                gp.setCurrency(rs.getString("currency"));
                gp.setDrivingPrice(rs.getInt("driving_price"));
                gp.setGuidingPrice(rs.getInt("guiding_price"));
                gp.setTime(rs.getInt("time"));

                guidePriceList.add(gp);
            }

            return guidePriceList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public GuidePrice getOne(GuidePrice guidePrice) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(GuidePriceSchemaSql.SQL_GUIDE_PRICE_GET_ONE)) {
            preparedStatement.setInt(1, guidePrice.getGuide().getId());
            preparedStatement.setInt(2, guidePrice.getPlace().getId());
            ResultSet rs = preparedStatement.executeQuery();

            GuidePrice gp = new GuidePrice();
            while (rs.next()) {
                gp.setId(rs.getInt("id"));
                gp.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                gp.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
                gp.setCurrency(rs.getString("currency"));
                gp.setDrivingPrice(rs.getInt("driving_price"));
                gp.setGuidingPrice(rs.getInt("guiding_price"));
                gp.setTime(rs.getInt("time"));
            }

            return gp;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public int save(GuidePrice guidePrice) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuidePriceSchemaSql.SQL_GUIDE_PRICE_SAVE)) {
            preparedStatement.setInt(1, guidePrice.getPlace().getId());
            preparedStatement.setInt(2, guidePrice.getGuide().getId());
            preparedStatement.setInt(3, guidePrice.getTime());
            preparedStatement.setDouble(4, guidePrice.getGuidingPrice());
            preparedStatement.setDouble(5, guidePrice.getDrivingPrice());
            preparedStatement.setString(6, guidePrice.getCurrency());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(GuidePrice guidePrice) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuidePriceSchemaSql.SQL_GUIDE_PRICE_UPDATE)) {
            preparedStatement.setInt(1, guidePrice.getPlace().getId());
            preparedStatement.setInt(2, guidePrice.getGuide().getId());
            preparedStatement.setInt(3, guidePrice.getTime());
            preparedStatement.setDouble(4, guidePrice.getGuidingPrice());
            preparedStatement.setDouble(5, guidePrice.getDrivingPrice());
            preparedStatement.setString(6, guidePrice.getCurrency());
            preparedStatement.setInt(7, guidePrice.getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(GuidePrice guidePrice) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GuidePriceSchemaSql.SQL_GUIDE_PRICE_DELETE)) {
            preparedStatement.setInt(1, guidePrice.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GuidePrice calculatePrice(Guide guide, Place place, IndexSearch indexSearch, Double g_price){

        GuidePrice gp = getOne(new GuidePrice(place, guide));
        Period period = Period.between(indexSearch.getCheckin(), indexSearch.getCheckout());
        int dayTime = period.getDays() + 1;

        if(period.isZero()){
            gp.setTime(1);
        }else if(!period.isZero() & !period.isNegative()){
            gp.setTime(dayTime);
            gp.setGuidingPrice((int) Math.round(dayTime * g_price));
        }

        return gp;
    }
}

package com.rentaguide.repository.DB;

import java.sql.Array;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Aim;
import com.rentaguide.entity.Guide;
import com.rentaguide.entity.IndexSearch;
import com.rentaguide.entity.Language;
import com.rentaguide.entity.Place;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SearchDao;
import com.rentaguide.repository.SQL.IndexSearchSchemaSql;

@Repository
public class IndexSearchDaoImpl implements SearchDao<IndexSearch> {

	public HKDataSource dataSource;
	public PlaceDaoImpl placeDao;
	public AimDaoImpl aimDao;
	public GuideDaoImpl guideDao;
	public GuidePriceDaoImpl guidePriceDao;
	public UserDaoImpl userDao;

	public IndexSearchDaoImpl(HKDataSource dataSource, PlaceDaoImpl placeDao, AimDaoImpl aimDao, GuideDaoImpl guideDao,
			GuidePriceDaoImpl guidePriceDao, UserDaoImpl userDao) {
		this.dataSource = dataSource;
		this.placeDao = placeDao;
		this.aimDao = aimDao;
		this.guideDao = guideDao;
		this.guidePriceDao = guidePriceDao;
		this.userDao = userDao;
	}

	@Override
	public List<IndexSearch> find(IndexSearch indexSearch) {
		try (Connection conn = dataSource.getConnection();
				
				PreparedStatement preparedStatement = conn
						.prepareStatement(IndexSearchSchemaSql.SQL_INDEXSEARCH_FIND)) {
			preparedStatement.setString(1, indexSearch.getDriverLicence());
			preparedStatement.setInt(2, indexSearch.getUser().getId());
			preparedStatement.setInt(3, indexSearch.getUser().getId());
			preparedStatement.setInt(4, indexSearch.getPlace().getId());
			preparedStatement.setInt(5, indexSearch.getPlace().getId());
			preparedStatement.setInt(6, indexSearch.getAim().getId());
			preparedStatement.setInt(7, indexSearch.getAim().getId());
			preparedStatement.setInt(8, indexSearch.getPlace().getId());
			preparedStatement.setInt(9, indexSearch.getPlace().getId());
			preparedStatement.setDate(10, Date.valueOf(indexSearch.getCheckin()));
			preparedStatement.setDate(11, Date.valueOf(indexSearch.getCheckout()));
			preparedStatement.setDate(12, Date.valueOf(indexSearch.getCheckin()));
			preparedStatement.setDate(13, Date.valueOf(indexSearch.getCheckout()));
			preparedStatement.setDate(14, Date.valueOf(indexSearch.getCheckin()));
			preparedStatement.setDate(15, Date.valueOf(indexSearch.getCheckout()));
			preparedStatement.setObject(16, indexSearch.getMinPrice(), Types.DOUBLE);
			preparedStatement.setObject(17, indexSearch.getMinPrice(), Types.DOUBLE);
			preparedStatement.setObject(18, indexSearch.getMaxPrice(), Types.DOUBLE);
			preparedStatement.setObject(19, indexSearch.getMaxPrice(), Types.DOUBLE);
			preparedStatement.setString(20, indexSearch.getGender());
			preparedStatement.setString(21, indexSearch.getGender());
			preparedStatement.setString(22, indexSearch.getDriverLicence());		
			
			System.out.println("langSize: "+  indexSearch.getLang().size());

			Language[] data = indexSearch.getLang().toArray(new Language[indexSearch.getLang().size()]);
			List<Integer> langIds = new ArrayList<>();
			  for(int i=0; i<data.length; i++){
			   langIds.add((Integer) data[i].getId());
			  }
			
			Array langIdsArray = conn.createArrayOf("integer", langIds.toArray());
			
			preparedStatement.setArray(23, langIdsArray);
			
			ResultSet rs = preparedStatement.executeQuery();
			List<IndexSearch> userList = new ArrayList<IndexSearch>();
			while (rs.next()) {
				IndexSearch is = new IndexSearch();
				is.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
				is.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
				Double guidePrice = rs.getDouble("g_price");
				is.setGuidePrice(guidePriceDao.calculatePrice(new Guide(rs.getInt("guide_id")),
						new Place(rs.getInt("place_id")), indexSearch, guidePrice));
				is.setAim(aimDao.getOne(indexSearch.getAim()));
				is.setMaxPrice(rs.getDouble("g_price"));
				

				userList.add(is);

			}

			return userList;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			System.out.println(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

		return null;
	}

	@Override
	public IndexSearch getOne(IndexSearch guide) {
		try (Connection conn = dataSource.getConnection();

				PreparedStatement preparedStatement = conn
						.prepareStatement(IndexSearchSchemaSql.SQL_INDEXSEARCH_GET_ONE)) {
			preparedStatement.setInt(1, guide.getId());
			ResultSet rs = preparedStatement.executeQuery();

			IndexSearch is = new IndexSearch();
			while (rs.next()) {
				is.setId(rs.getInt("id"));
				is.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
				is.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));
				is.setCheckin(rs.getObject("checkin", LocalDate.class));
				is.setCheckout(rs.getObject("checkout", LocalDate.class));
				is.setSearchdate(rs.getObject("search_date", LocalDate.class));
				is.setGender(rs.getString("gender"));
				is.setDriverLicence(rs.getString("driver_licence"));
				is.setMinPrice(rs.getDouble("min_price"));
				is.setMaxPrice(rs.getDouble("max_price"));
				is.setCurrency(rs.getString("currency"));
			}

			return is;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int save(IndexSearch indexSearch) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(IndexSearchSchemaSql.SQL_INDEXSEARCH_SAVE)) {
			preparedStatement.setInt(1, indexSearch.getPlace().getId());
			preparedStatement.setInt(2, indexSearch.getAim().getId());
			preparedStatement.setObject(3, indexSearch.getCheckin());
			preparedStatement.setObject(4, indexSearch.getCheckout());
			preparedStatement.setObject(5, indexSearch.getSearchdate());
			preparedStatement.setString(6, indexSearch.getGender());
			preparedStatement.setString(7, indexSearch.getDriverLicence());
			preparedStatement.setDouble(8, indexSearch.getMinPrice());
			preparedStatement.setDouble(9, indexSearch.getMaxPrice());
			preparedStatement.setString(10, indexSearch.getCurrency());

			return preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int update(IndexSearch indexSearch) {
		return 0;

	}

	@Override
	public void delete(IndexSearch indexSearch) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(IndexSearchSchemaSql.SQL_INDEXSEARCH_DELETE)) {
			preparedStatement.setInt(1, indexSearch.getId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static  ArrayList<Integer> convert(Language[] objectArray){
		  Integer[] intArray = new Integer[objectArray.length];
		  ArrayList<Integer> array_list_ids = new ArrayList<Integer>();

		  for(int i=0; i<objectArray.length; i++){
		   intArray[i] = (Integer) objectArray[i].getId();
		   array_list_ids.add(objectArray[i].getId());
		  // System.out.println("intArray["+i+"] = "+intArray[i] );
		  }

		  return array_list_ids;
		 }
}

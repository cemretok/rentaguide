package com.rentaguide.repository.DB.Interfaces;

import java.util.List;

import com.rentaguide.entity.PaymentCreation;
import com.rentaguide.entity.PaymentResponse;

public interface PaymentDao<T> {

	List<T> getAll();

    T getOne(T t);

    int save(T t);

    int update(T t);

    void delete(T t);
    
	PaymentResponse makePayment(PaymentCreation pc);
}
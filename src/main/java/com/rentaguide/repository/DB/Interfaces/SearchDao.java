package com.rentaguide.repository.DB.Interfaces;

import java.util.List;

public interface SearchDao<T> {

    List<T> find(T t);

    T getOne(T t);

    int save(T t);

    int update(T t);

    void delete(T t);
}
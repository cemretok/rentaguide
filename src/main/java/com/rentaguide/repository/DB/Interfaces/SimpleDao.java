package com.rentaguide.repository.DB.Interfaces;

import java.util.List;

public interface SimpleDao<T> {

    List<T> getAll();

    T getOne(T t);

    int save(T t);

    int update(T t);

    void delete(T t);
    
}

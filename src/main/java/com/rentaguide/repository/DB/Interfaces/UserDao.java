package com.rentaguide.repository.DB.Interfaces;

import java.util.List;

public interface UserDao<T> {

    List<T> find(T t);

    T getOne(T t);

    T findByMail(String mail);

    int save(T t);

    int update(T t);

    void delete(T t);
}
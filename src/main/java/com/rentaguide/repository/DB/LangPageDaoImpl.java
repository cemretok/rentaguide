package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.LanguagePage;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.LanguagePageSchemaSql;

@Repository
public class LangPageDaoImpl implements SimpleDao<LanguagePage> {

	public HKDataSource dataSource;

	public LangPageDaoImpl(HKDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<LanguagePage> getAll() {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(LanguagePageSchemaSql.SQL_LANG_PAGE_GET_ALL)) {

			ResultSet rs = preparedStatement.executeQuery();
			List<LanguagePage> langPageList = new ArrayList<LanguagePage>();
			while (rs.next()) {
				LanguagePage langPage = new LanguagePage();
				langPage.setLanguageId(rs.getInt("language_id"));
				langPage.setLanguageName(rs.getString("language_name"));
				langPage.setLanguageDesc(rs.getString("language_desc"));

				langPageList.add(langPage);
			}

			return langPageList;

		} catch (Exception e) {
			System.err.format("SQL Cause: %s\n%s", e.getCause(), e.getMessage());
		}
		return null;
	}

	@Override
	public LanguagePage getOne(LanguagePage t) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(LanguagePageSchemaSql.SQL_LANG_PAGE_GET_ONE)) {

			preparedStatement.setInt(1, t.getLanguageId());
			ResultSet rs = preparedStatement.executeQuery();
			LanguagePage langPage = new LanguagePage();
			langPage.setLanguageId(rs.getInt("language_id"));
			langPage.setLanguageName(rs.getString("language_desc"));
			langPage.setLanguageDesc(rs.getString("language_name"));

			return langPage;

		} catch (Exception e) {
			System.err.format("SQL Cause: %s\n%s", e.getCause(), e.getMessage());
		}
		return null;
	}

	@Override
	public int save(LanguagePage t) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn.prepareStatement(LanguagePageSchemaSql.SQL_LANG_PAGE_SAVE)) {

			preparedStatement.setInt(1, t.getLanguageId());
			preparedStatement.setString(2, t.getLanguageName());
			preparedStatement.setString(3, t.getLanguageDesc());

			return preparedStatement.executeUpdate();

		} catch (Exception e) {
			System.err.format("SQL Cause: %s\n%s", e.getCause(), e.getMessage());
		}

		return 0;
	}

	@Override
	public int update(LanguagePage t) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(LanguagePageSchemaSql.SQL_LANG_PAGE_UPDATE)) {

			preparedStatement.setInt(1, t.getLanguageId());
			preparedStatement.setString(2, t.getLanguageName());
			preparedStatement.setString(3, t.getLanguageDesc());

			return preparedStatement.executeUpdate();

		} catch (Exception e) {
			System.err.format("SQL Cause: %s\n%s", e.getCause(), e.getMessage());
		}
		return 0;
	}

	@Override
	public void delete(LanguagePage t) {

		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(LanguagePageSchemaSql.SQL_LANG_PAGE_DELETE)) {
			preparedStatement.setInt(1, t.getLanguageId());

			preparedStatement.executeUpdate();
		} catch (Exception e) {
			System.err.format("SQL Cause: %s\n%s", e.getCause(), e.getMessage());
		}

	}

}

package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Language;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.LanguageSchemaSql;

@Repository
public class LanguageDaoImpl implements SimpleDao<Language> {

    public HKDataSource dataSource;

    public LanguageDaoImpl(HKDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Language> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(LanguageSchemaSql.SQL_LANGUAGE_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<Language> languageList = new ArrayList<Language>();
            while (rs.next()) {
                Language a = new Language();

                a.setId(rs.getInt("id"));
                a.setName(rs.getString("name"));

                languageList.add(a);
            }

            return languageList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Language getOne(Language language) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(LanguageSchemaSql.SQL_LANGUAGE_GET_ONE)) {
            preparedStatement.setInt(1, language.getId());
            ResultSet rs = preparedStatement.executeQuery();
            Language l = new Language();
            while (rs.next()) {
                l.setId(rs.getInt("id"));
                l.setName(rs.getString("name"));
            }

            return l;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int save(Language language) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(LanguageSchemaSql.SQL_LANGUAGE_SAVE)) {
            preparedStatement.setString(1, language.getName());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(Language language) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(LanguageSchemaSql.SQL_LANGUAGE_UPDATE)) {
            preparedStatement.setString(1, language.getName());
            preparedStatement.setInt(2, language.getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(Language language) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(LanguageSchemaSql.SQL_LANGUAGE_DELETE)) {
            preparedStatement.setInt(1, language.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

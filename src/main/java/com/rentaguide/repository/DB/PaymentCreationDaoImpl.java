package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.PaymentCreation;
import com.rentaguide.entity.PaymentResponse;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.PaymentDao;
import com.rentaguide.repository.SQL.PaymentCreationSchemaSql;

@Repository
public class PaymentCreationDaoImpl implements PaymentDao<PaymentCreation>{
	
	public HKDataSource dataSource;
	
	public PaymentCreationDaoImpl(HKDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<PaymentCreation> getAll() {
		try(Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn.prepareStatement(PaymentCreationSchemaSql.SQL_PAYMENT_GET_ALL)
				) {
			
			ResultSet rs = preparedStatement.executeQuery();
			List<PaymentCreation> paymentList = new ArrayList<PaymentCreation>();
			while(rs.next()) {
				PaymentCreation pc = new PaymentCreation();
				
				pc.setId(rs.getInt("id"));
				pc.setCardHolder(rs.getString("cardHolder"));
				pc.setCardNumber(rs.getString("cardNumber"));
				pc.setExpirationMonth(rs.getString("expirationMonth"));
				pc.setExpirationYear(rs.getString("expirationYear"));
				pc.setCvcNumber(rs.getString("cvcNumber"));
				
				paymentList.add(pc);
			}
			return paymentList;
			
		} catch (SQLException e) {
			 System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}

	@Override
	public PaymentCreation getOne(PaymentCreation pc) {
		try (Connection conn = dataSource.getConnection();
			PreparedStatement preparedStatement = conn.prepareStatement(PaymentCreationSchemaSql.SQL_PAYMENT_GET_ONE);	) {
			
			preparedStatement.setInt(1, pc.getId());
			ResultSet rs = preparedStatement.executeQuery();
			PaymentCreation payCreation = new PaymentCreation();
			
			while(rs.next()) {
				
				payCreation.setId(rs.getInt("id"));
				payCreation.setCardHolder(rs.getString("cardHolder"));
				payCreation.setCardNumber(rs.getString("cardNumber"));
				payCreation.setExpirationMonth(rs.getString("expirationMonth"));
				payCreation.setExpirationYear(rs.getString("expirationYear"));
				payCreation.setCvcNumber(rs.getString("cvcNumber"));
			}
			
			return payCreation;
		} catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}

	@Override
	public int save(PaymentCreation t) {
		try (Connection conn = dataSource.getConnection();
	             PreparedStatement preparedStatement = conn.prepareStatement(PaymentCreationSchemaSql.SQL_PAYMENT_SAVE)) {
			
			preparedStatement.setInt(1, t.getReservationId());
			preparedStatement.setString(2, t.getCardHolder());
			preparedStatement.setString(3, t.getCardNumber());
			preparedStatement.setString(4, t.getExpirationMonth());
			preparedStatement.setString(5, t.getExpirationYear());
			preparedStatement.setString(6, t.getCvcNumber());
			
			return preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return 0;
	}

	@Override
	public int update(PaymentCreation t) {
		try (Connection conn = dataSource.getConnection();
	             PreparedStatement preparedStatement = conn.prepareStatement(PaymentCreationSchemaSql.SQL_PAYMENT_UPDATE)){
			
			preparedStatement.setInt(1, t.getReservationId());
			preparedStatement.setString(2, t.getCardHolder());
			preparedStatement.setString(3, t.getCardNumber());
			preparedStatement.setString(4, t.getExpirationMonth());
			preparedStatement.setString(5, t.getExpirationYear());
			preparedStatement.setString(6, t.getCvcNumber());
			
			return preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return 0;
	}

	@Override
	public void delete(PaymentCreation t) {
		
		try (Connection conn = dataSource.getConnection();
	             PreparedStatement preparedStatement = conn.prepareStatement(PaymentCreationSchemaSql.SQL_PAYMENT_DELETE)){
			
			preparedStatement.setInt(1, t.getId());
			
			preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		
	}

	@Override
	public PaymentResponse makePayment(PaymentCreation pc) {
		// TODO Auto-generated method stub
		return null;
		
	}
	

}

package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.PaymentResponse;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.PaymentResponseSchemaSql;

@Repository
public class PaymentResponseDaoImpl implements SimpleDao<PaymentResponse> {

	public HKDataSource dataSource;

	public PaymentResponseDaoImpl(HKDataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<PaymentResponse> getAll() {
		try (Connection conn = dataSource.getConnection();

				PreparedStatement preparedStatement = conn
						.prepareStatement(PaymentResponseSchemaSql.SQL_PAYMENT_GET_ALL)) {

			ResultSet rs = preparedStatement.executeQuery();
			List<PaymentResponse> paymentResponseList = new ArrayList<PaymentResponse>();
			while (rs.next()) {
				PaymentResponse a = new PaymentResponse();

				a.setId(rs.getInt("id"));
				a.setStatus(rs.getString("status"));
				a.setDescription(rs.getString("description"));

				paymentResponseList.add(a);
			}

			return paymentResponseList;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public PaymentResponse getOne(PaymentResponse t) {
		try (Connection conn = dataSource.getConnection();

				PreparedStatement preparedStatement = conn
						.prepareStatement(PaymentResponseSchemaSql.SQL_PAYMENT_GET_ONE)) {
			preparedStatement.setInt(1, t.getId());
			ResultSet rs = preparedStatement.executeQuery();
			PaymentResponse l = new PaymentResponse();
			while (rs.next()) {
				l.setId(rs.getInt("id"));
				l.setStatus(rs.getString("status"));
				l.setDescription(rs.getString("description"));
			}

			return l;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int save(PaymentResponse t) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(PaymentResponseSchemaSql.SQL_PAYMENT_SAVE)) {
			preparedStatement.setString(1, t.getStatus());
			preparedStatement.setString(2, t.getDescription());

			return preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int update(PaymentResponse t) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(PaymentResponseSchemaSql.SQL_PAYMENT_UPDATE)) {
			preparedStatement.setString(1, t.getStatus());
			preparedStatement.setString(2, t.getDescription());

			return preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void delete(PaymentResponse t) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(PaymentResponseSchemaSql.SQL_PAYMENT_DELETE)) {
			preparedStatement.setInt(1, t.getId());

			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

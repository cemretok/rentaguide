package com.rentaguide.repository.DB;

import com.rentaguide.entity.Place;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.SQL.PlaceSchemaSql;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PlaceDaoImpl implements SimpleDao<Place> {

    public HKDataSource dataSource;

    public PlaceDaoImpl(HKDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Place> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(PlaceSchemaSql.SQL_PLACE_GET_ALL)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<Place> placeList = new ArrayList<Place>();
            while (rs.next()) {
                Place p = new Place();
                p.setId(rs.getInt("id"));
                p.setCountry(rs.getString("country"));
                p.setDistrict(rs.getString("district"));
                p.setProvince(rs.getString("province"));

                placeList.add(p);
            }

            return placeList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Place getOne(Place place) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(PlaceSchemaSql.SQL_PLACE_GET_ONE)) {
            preparedStatement.setInt(1, place.getId());
            ResultSet rs = preparedStatement.executeQuery();
            Place l = new Place();
            while (rs.next()) {
                l.setId(rs.getInt("id"));
                l.setCountry(rs.getString("country"));
                l.setDistrict(rs.getString("district"));
                l.setProvince(rs.getString("province"));
            }

            return l;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int save(Place place) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PlaceSchemaSql.SQL_PLACE_SAVE)) {
            preparedStatement.setString(1, place.getCountry());
            preparedStatement.setString(2, place.getDistrict());
            preparedStatement.setString(3, place.getProvince());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(Place place) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PlaceSchemaSql.SQL_PLACE_UPDATE)) {
            preparedStatement.setString(1, place.getProvince());
            preparedStatement.setString(2, place.getDistrict());
            preparedStatement.setString(3, place.getCountry());
            preparedStatement.setInt(4, place.getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(Place place) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PlaceSchemaSql.SQL_PLACE_DELETE)) {
            preparedStatement.setInt(1, place.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

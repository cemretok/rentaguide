package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Aim;
import com.rentaguide.entity.Guide;
import com.rentaguide.entity.Place;
import com.rentaguide.entity.Reservation;
import com.rentaguide.entity.User;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.ReservationSchemaSql;

@Repository
public class ReservationDaoImpl implements SimpleDao<Reservation> {

    public HKDataSource dataSource;
    public GuideDaoImpl guideDao;
    public UserDaoImpl userDao;
    public PlaceDaoImpl placeDao;
    public AimDaoImpl aimDao;

    public ReservationDaoImpl(HKDataSource dataSource, GuideDaoImpl guideDao, UserDaoImpl userDao, PlaceDaoImpl placeDao, AimDaoImpl aimDao) {
        this.dataSource = dataSource;
        this.guideDao = guideDao;
        this.userDao = userDao;
        this.placeDao = placeDao;
        this.aimDao = aimDao;
    }

    @Override
    public List<Reservation> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_GET_ALL_RESERVATIONS)) {

            ResultSet rs = preparedStatement.executeQuery();
            List<Reservation> reservationList = new ArrayList<Reservation>();
            while (rs.next()) {
                Reservation r = new Reservation();

                r.setId(rs.getInt("id"));
                r.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                r.setUser(userDao.getOne(new User(rs.getInt("user_id"))));
                r.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
                r.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));
                r.setReservationStartDate(rs.getObject("start_date", LocalDateTime.class));
                r.setReservationEndDate(rs.getObject("end_date", LocalDateTime.class));
                r.setTotalPrice(rs.getDouble("total_price"));
                r.setReservationStatus(rs.getString("reservation_status"));
                r.setNotes(rs.getString("notes"));
                reservationList.add(r);
            }

            return reservationList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    
    public List<Reservation> getAllForUsers(Integer userId) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_GET_ALL_USER_ID)) {
        	preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            List<Reservation> reservationList = new ArrayList<Reservation>();
            while (rs.next()) {
                Reservation r = new Reservation();

                r.setId(rs.getInt("id"));
                r.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                r.setUser(userDao.getOne(new User(rs.getInt("user_id"))));
                r.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
                r.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));
                r.setReservationStartDate(rs.getObject("start_date", LocalDateTime.class));
                r.setReservationEndDate(rs.getObject("end_date", LocalDateTime.class));
                r.setTotalPrice(rs.getDouble("total_price"));
                r.setReservationStatus(rs.getString("reservation_status"));
                r.setNotes(rs.getString("notes"));
                reservationList.add(r);
            }

            return reservationList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public List<Reservation> getAllForGuides(Integer guideId) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_GET_ALL_GUIDE_ID)) {
        	preparedStatement.setInt(1, guideId);
            ResultSet rs = preparedStatement.executeQuery();
            List<Reservation> reservationList = new ArrayList<Reservation>();
            while (rs.next()) {
                Reservation r = new Reservation();

                r.setId(rs.getInt("id"));
                r.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                r.setUser(userDao.getOne(new User(rs.getInt("user_id"))));
                r.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
                r.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));
                r.setReservationStartDate(rs.getObject("start_date", LocalDateTime.class));
                r.setReservationEndDate(rs.getObject("end_date", LocalDateTime.class));
                r.setTotalPrice(rs.getDouble("total_price"));
                r.setReservationStatus(rs.getString("reservation_status"));
                r.setNotes(rs.getString("notes"));
                reservationList.add(r);
            }

            return reservationList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Reservation getOne(Reservation reservation) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_GET_ONE)) {
            preparedStatement.setInt(1, reservation.getId());
            ResultSet rs = preparedStatement.executeQuery();
            Reservation r = new Reservation();
            while (rs.next()) {

                r.setId(rs.getInt("id"));
                r.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
                r.setUser(userDao.getOne(new User(rs.getInt("user_id"))));
                r.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
                r.setAim(aimDao.getOne(new Aim(rs.getInt("aim_id"))));
                r.setReservationStartDate(rs.getObject("start_date", LocalDateTime.class));
                r.setReservationEndDate(rs.getObject("end_date", LocalDateTime.class));
                r.setTotalPrice(rs.getDouble("total_price"));
                r.setReservationStatus(rs.getString("reservation_status"));
                r.setNotes(rs.getString("notes"));

            }

            return r;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    

    @Override
    public int save(Reservation reservation) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_SAVE)) {

            preparedStatement.setInt(1, reservation.getGuide().getId());
            preparedStatement.setInt(2, reservation.getUser().getId());
            preparedStatement.setInt(3, reservation.getPlace().getId());
            preparedStatement.setInt(4, reservation.getAim().getId());
            preparedStatement.setObject(5, reservation.getReservationStartDate());
            preparedStatement.setObject(6, reservation.getReservationEndDate());
            preparedStatement.setDouble(7, reservation.getTotalPrice());
            preparedStatement.setString(8, reservation.getReservationStatus());
            preparedStatement.setString(9, reservation.getNotes());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(Reservation reservation) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_UPDATE)) {
            preparedStatement.setDouble(1, reservation.getTotalPrice());
            preparedStatement.setString(2, reservation.getReservationStatus());
            preparedStatement.setString(3, reservation.getNotes());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(Reservation reservation) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(ReservationSchemaSql.SQL_RESERVATION_DELETE)) {
            preparedStatement.setInt(1, reservation.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

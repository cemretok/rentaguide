package com.rentaguide.repository.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.rentaguide.entity.Guide;
import com.rentaguide.entity.Place;
import com.rentaguide.entity.Timesheet;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.SQL.TimesheetSchemaSql;

@Repository
@Component
public class TimesheetDaoImpl implements SimpleDao<Timesheet> {

	public HKDataSource dataSource;
	public GuideDaoImpl guideDao;
	public PlaceDaoImpl placeDao;

	public TimesheetDaoImpl(HKDataSource dataSource, GuideDaoImpl guideDao, PlaceDaoImpl placeDao) {
		this.dataSource = dataSource;
		this.guideDao = guideDao;
		this.placeDao = placeDao;
	}

	@Override
	public List<Timesheet> getAll() {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn.prepareStatement(TimesheetSchemaSql.SQL_TIMESHEET_GET_ALL)) {
			ResultSet rs = preparedStatement.executeQuery();
			List<Timesheet> timesheetList = new ArrayList<Timesheet>();
			while (rs.next()) {
				Timesheet ts = new Timesheet();

				ts.setId(rs.getInt("id"));
				ts.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
				ts.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
				ts.setEndDate(rs.getObject("start_date", LocalDateTime.class));
				ts.setEndDate(rs.getObject("end_date", LocalDateTime.class));
				ts.setStatus(rs.getString("status"));
				timesheetList.add(ts);
			}

			return timesheetList;
		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Timesheet getOne(Timesheet timesheet) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn
						.prepareStatement(TimesheetSchemaSql.SQL_TIMESHEET_ONE)) {

			preparedStatement.setInt(1, timesheet.getId());
			ResultSet rs = preparedStatement.executeQuery();
			Timesheet ts = new Timesheet();
			while (rs.next()) {
				ts.setId(rs.getInt("id"));
				ts.setGuide(guideDao.getOne(new Guide(rs.getInt("guide_id"))));
				ts.setPlace(placeDao.getOne(new Place(rs.getInt("place_id"))));
				ts.setEndDate(rs.getObject("start_date", LocalDateTime.class));
				ts.setEndDate(rs.getObject("end_date", LocalDateTime.class));
				ts.setStatus(rs.getString("status"));
			}

			return ts;

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public int save(Timesheet timesheet) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn.prepareStatement(TimesheetSchemaSql.SQL_TIMESHEET_SAVE)) {

			preparedStatement.setInt(1, timesheet.getGuide().getId());
			preparedStatement.setInt(2, timesheet.getPlace().getId());
			preparedStatement.setObject(3, timesheet.getStartDate());
			preparedStatement.setObject(4, timesheet.getEndDate());
			preparedStatement.setString(5, timesheet.getStatus());

			return preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int update(Timesheet timesheet) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn.prepareStatement(TimesheetSchemaSql.SQL_TIMESHEET_UPDATE)) {
			preparedStatement.setString(1, timesheet.getStatus());
			return preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void delete(Timesheet timehseet) {
		try (Connection conn = dataSource.getConnection();
				PreparedStatement preparedStatement = conn.prepareStatement(TimesheetSchemaSql.SQL_TIMESHEET_DELETE)) {

			preparedStatement.setInt(1, timehseet.getId());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

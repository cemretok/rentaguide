package com.rentaguide.repository.DB;

import com.rentaguide.entity.User;
import com.rentaguide.repository.DB.Interfaces.UserDao;
import com.rentaguide.repository.HKDataSource;
import com.rentaguide.repository.SQL.UserSchemaSql;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao<User> {

    public HKDataSource dataSource;

    public UserDaoImpl(HKDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<User> find(User t) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(UserSchemaSql.SQL_USER_FIND)) {
            System.out.println("t:" + t.toString());
            preparedStatement.setObject(1, t.getId(), java.sql.Types.INTEGER);
            preparedStatement.setObject(2, t.getMail(), java.sql.Types.VARCHAR);
            preparedStatement.setObject(3, t.getPassportNo(), java.sql.Types.VARCHAR);

            ResultSet rs = preparedStatement.executeQuery();
            List<User> userList = new ArrayList<User>();
            while (rs.next()) {
                User u = new User();

                u.setId(rs.getInt("id"));
                u.setMail(rs.getString("mail"));
                u.setName(rs.getString("name"));
                u.setPassportNo(rs.getString("passport_no"));
                u.setPassword(rs.getString("password"));
                u.setPhone(rs.getString("phone"));
                u.setRegisterDate(rs.getObject("register_date", LocalDateTime.class));
                u.setSurname(rs.getString("surname"));

                userList.add(u);
            }

            return userList;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User getOne(User user) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement preparedStatement = conn.prepareStatement(UserSchemaSql.SQL_USER_GET_ONE)) {
            preparedStatement.setInt(1, user.getId());
            ResultSet rs = preparedStatement.executeQuery();

            User u = new User();
            while (rs.next()) {
                u.setId(rs.getInt("id"));
                u.setPassportNo(rs.getString("passport_no"));
                u.setMail(rs.getString("mail"));
                u.setName(rs.getString("name"));
                u.setPhone(rs.getString("phone"));
                u.setRegisterDate(rs.getObject("register_date", LocalDateTime.class));
                u.setSurname(rs.getString("surname"));
            }

            return u;

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public User findByMail(String mail) {
        User u = new User();
        u.setMail(mail);
        List<User> userList = find(u);

        if (userList.size() > 0) {
            return userList.get(0);
        }
        else if(userList.size() == 0){
            return null;
        }

        return null;
    }

    @Override
    public int save(User t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(UserSchemaSql.SQL_USER_SAVE)) {
            preparedStatement.setString(1, t.getMail());
            preparedStatement.setString(2, t.getName());
            preparedStatement.setString(3, t.getPassportNo());
            preparedStatement.setString(4, t.getPassword());
            preparedStatement.setString(5, t.getPhone());
            preparedStatement.setObject(6, t.getRegisterDate());
            preparedStatement.setString(7, t.getSurname());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int update(User t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(UserSchemaSql.SQL_USER_UPDATE)) {
            preparedStatement.setString(1, t.getName());
            preparedStatement.setString(2, t.getSurname());
            preparedStatement.setString(3, t.getPassportNo());
            preparedStatement.setString(4, t.getPhone());
            preparedStatement.setInt(5, t.getId());

            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;

    }

    @Override
    public void delete(User t) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(UserSchemaSql.SQL_USER_DELETE)) {
            preparedStatement.setInt(1, t.getId());

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.rentaguide.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;

@Service
public class HKDataSource {

    private static HikariConfig config = new HikariConfig();
    private static HikariDataSource ds;

    static {
        config.setJdbcUrl("jdbc:postgresql://46.101.219.228:54320/postgres?searchpath=rentaguide");
        config.setSchema("rentaguide");
        config.setUsername("postgres");
        config.setMaximumPoolSize(10);
        config.setMaxLifetime(30000);
        config.setPassword("s1B366199ZrFa3");
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        ds = new HikariDataSource(config);
    }

    private HKDataSource() {
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}

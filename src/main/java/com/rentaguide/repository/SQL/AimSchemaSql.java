package com.rentaguide.repository.SQL;

public final class AimSchemaSql {

    public static final String SQL_AIM_SAVE = "INSERT INTO AIM (aim,aim_detail) values (?,?)";
    public static final String SQL_AIM_DELETE = "DELETE FROM AIM WHERE id = ?";
    public static final String SQL_AIM_GET_ALL = "SELECT * FROM AIM";
    public static final String SQL_AIM_GET_ONE = "SELECT * FROM AIM WHERE id = ?";
    public static final String SQL_AIM_UPDATE = "UPDATE AIM SET aim = ?, aim_detail = ? WHERE id = ?";

    private AimSchemaSql() {
    }

}

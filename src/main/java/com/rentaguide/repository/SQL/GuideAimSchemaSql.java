package com.rentaguide.repository.SQL;

public final class GuideAimSchemaSql {

    public static final String SQL_GUIDE_AIM_SAVE = "INSERT INTO GUIDE_AIM (guide_id,aim_id) values (?,?)";
    public static final String SQL_GUIDE_AIM_DELETE = "DELETE FROM GUIDE_AIM WHERE guide_id = ? and aim_id = ?";
    public static final String SQL_GUIDE_AIM_GET_ALL = "SELECT * FROM GUIDE_AIM";
    public static final String SQL_GUIDE_AIM_GET_ONE = "SELECT * FROM GUIDE_AIM WHERE guide_id = ? and aim_id = ?";
    public static final String SQL_GUIDE_AIM_UPDATE = "UPDATE GUIDE_AIM SET guide_id = ?, aim_id = ? WHERE  guide_id = ? and aim_id = ?";

    private GuideAimSchemaSql() {
    }

}

package com.rentaguide.repository.SQL;

public final class GuideLanguageSchemaSql {

    public static final String SQL_GUIDE_LANGUAGE_SAVE = "INSERT INTO GUIDE_LANGUAGE (guide_id,language_id) values (?,?)";
    public static final String SQL_GUIDE_LANGUAGE_DELETE = "DELETE FROM GUIDE_LANGUAGE WHERE guide_id = ? and language_id = ?";
    public static final String SQL_GUIDE_LANGUAGE_GET_ALL = "SELECT * FROM GUIDE_LANGUAGE";
    public static final String SQL_GUIDE_LANGUAGE_GET_ONE = "SELECT * FROM GUIDE_LANGUAGE WHERE guide_id = ? and language_id = ?";
    public static final String SQL_GUIDE_LANGUAGE_UPDATE = "UPDATE GUIDE_LANGUAGE SET guide_id = ?, language_id = ? WHERE  guide_id = ? and language_id = ?";

    private GuideLanguageSchemaSql() {
    }

}

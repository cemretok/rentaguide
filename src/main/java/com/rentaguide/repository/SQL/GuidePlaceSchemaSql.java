package com.rentaguide.repository.SQL;

public final class GuidePlaceSchemaSql {

    public static final String SQL_GUIDE_PLACE_SAVE = "INSERT INTO GUIDE_PLACE (guide_id,place_id) values (?,?)";
    public static final String SQL_GUIDE_PLACE_DELETE = "DELETE FROM GUIDE_PLACE WHERE guide_id = ? and place_id = ?";
    public static final String SQL_GUIDE_PLACE_GET_ALL = "SELECT * FROM GUIDE_PLACE";
    public static final String SQL_GUIDE_PLACE_GET_ONE = "SELECT * FROM GUIDE_PLACE WHERE guide_id = ? and place_id = ?";
    public static final String SQL_GUIDE_PLACE_UPDATE = "UPDATE GUIDE_PLACE SET guide_id = ?, place_id = ? WHERE  guide_id = ? and place_id = ?";

    private GuidePlaceSchemaSql() {
    }

}

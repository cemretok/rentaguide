package com.rentaguide.repository.SQL;

public final class GuidePriceSchemaSql {

    public static final String SQL_GUIDE_PRICE_SAVE = "INSERT INTO GUIDE_PRICE (place_id,guide_id,time,guiding_price,driving_price,currency) values (?,?,?,?,?,?)";
    public static final String SQL_GUIDE_PRICE_DELETE = "DELETE FROM GUIDE_PRICE WHERE id = ?";
    public static final String SQL_GUIDE_PRICE_GET_ALL = "SELECT * FROM GUIDE_PRICE";
    public static final String SQL_GUIDE_PRICE_GET_ONE = "SELECT * FROM GUIDE_PRICE WHERE guide_id = ? and place_id=?";
    public static final String SQL_GUIDE_PRICE_UPDATE = "UPDATE GUIDE_PRICE SET place_id = ?, guide_id = ?, time = ?, guiding_price = ?, driving_price = ?, currency = ? WHERE id = ?";

    private GuidePriceSchemaSql() {
    }

}

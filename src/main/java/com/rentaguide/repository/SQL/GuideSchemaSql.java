package com.rentaguide.repository.SQL;

public final class GuideSchemaSql {

	//public static final String SQL_GUIDE_SAVE = "INSERT INTO GUIDE (driver_licence,gender,mail,name,password,phone,picture,profile,register_date,surname,profilePicPath) values (?,?,?,?,?,?,?,?,?,?,?)";
    public static final String SQL_GUIDE_SAVE = "INSERT INTO GUIDE (driver_licence,gender,mail,name,password,phone,picture,profile,register_date,surname) values (?,?,?,?,?,?,?,?,?,?)";
    public static final String SQL_GUIDE_DELETE = "DELETE FROM GUIDE WHERE id = ?";
    public static final String SQL_GUIDE_FIND = "SELECT * FROM GUIDE WHERE id = ? or mail = ?";
    public static final String SQL_GUIDE_GET_ONE = "SELECT * FROM GUIDE WHERE id = ?";
    public static final String SQL_GUIDE_UPDATE = "UPDATE GUIDE SET name = ?, surname = ?, phone = ?, picture = ?, profile = ? , profilePicPath = ? WHERE id = ?";

    private GuideSchemaSql() {
    }

}

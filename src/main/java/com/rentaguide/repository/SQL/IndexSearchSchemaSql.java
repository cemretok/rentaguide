package com.rentaguide.repository.SQL;

public final class IndexSearchSchemaSql  {
    public static final String SQL_INDEXSEARCH_SAVE = "INSERT INTO INDEX_SEARCH (place_id,aim_id,checkin,checkout,search_date,gender,driver_licence, min_price,max_price, currency) values (?,?,?,?,?,?,?,?,?,?)";
    public static final String SQL_INDEXSEARCH_DELETE = "DELETE FROM INDEX_SEARCH WHERE id = ?";
    public static final String SQL_INDEXSEARCH_GET_ONE = "SELECT * FROM INDEX_SEARCH WHERE id = ?";
    public static final Double rate = 0.9;

    public static String SQL_INDEXSEARCH_FIND = "select\r\n" + 
    		"	g.id guide_id,\r\n" + 
    		"	gpl.place_id,\r\n" + 
    		"	g.gender,\r\n" + 
    		"	(case\r\n" + 
    		"		when '1' = ? \r\n" + 
    		"		then\r\n" + 
    		"		(case\r\n" + 
    		"			when (\r\n" + 
    		"				select count(1)\r\n" + 
    		"			from\r\n" + 
    		"				rentaguide.users u\r\n" + 
    		"			where\r\n" + 
    		"				u.id = ? ) > 0 then (gp.guiding_price + gp.driving_price)*" +rate+"\r\n" + 
    		"			else gp.guiding_price + gp.driving_price\r\n" + 
    		"		end )\r\n" + 
    		"		else\r\n" + 
    		"		(case\r\n" + 
    		"			when (\r\n" + 
    		"				select count(1)\r\n" + 
    		"			from\r\n" + 
    		"				rentaguide.users u\r\n" + 
    		"			where\r\n" + 
    		"				u.id = ? ) > 0 then gp.guiding_price* "+rate+"\r\n" + 
    		"			else gp.guiding_price\r\n" + 
    		"		end )\r\n" + 
    		"	end) g_price\r\n" + 
    		"from\r\n" + 
    		"	guide g,\r\n" + 
    		"	guide_place gpl,\r\n" + 
    		"	guide_price gp,\r\n" + 
    		"	guide_language gl\r\n" + 
    		"where\r\n" + 
    		"	1 = 1\r\n" + 
    		"	and g.id = gpl.guide_id\r\n" + 
    		"	and gp.guide_id = g.id\r\n" + 
    		"	and gp.place_id = gpl.place_id\r\n" + 
    		"	and (gpl.place_id = ?\r\n" + 
    		"	or ? = 0)\r\n" + 
    		"	and exists (\r\n" + 
    		"	select\r\n" + 
    		"		1\r\n" + 
    		"	from\r\n" + 
    		"		guide_aim ga,\r\n" + 
    		"		aim a\r\n" + 
    		"	where\r\n" + 
    		"		a.id = ga.aim_id\r\n" + 
    		"		and ga.guide_id = g.id\r\n" + 
    		"		and (a.id = (?)\r\n" + 
    		"		or 0 = ?) )\r\n" + 
    		"	and exists(\r\n" + 
    		"	select\r\n" + 
    		"		1\r\n" + 
    		"	from\r\n" + 
    		"		place p,\r\n" + 
    		"		guide_place gp\r\n" + 
    		"	where\r\n" + 
    		"		p.id = gp.place_id\r\n" + 
    		"		and gp.guide_id = g.id\r\n" + 
    		"		and (p.id = ?\r\n" + 
    		"		or 0 = ? ) )\r\n" + 
    		"	and not exists(\r\n" + 
    		"	select\r\n" + 
    		"		1\r\n" + 
    		"	from\r\n" + 
    		"		reservation r\r\n" + 
    		"	where\r\n" + 
    		"		r.guide_id = g.id\r\n" + 
    		"		and r.reservation_status = 'belirlenecek' )\r\n" + 
    		"	and not exists(\r\n" + 
    		"	select\r\n" + 
    		"		1\r\n" + 
    		"	from\r\n" + 
    		"		timesheet t\r\n" + 
    		"	where\r\n" + 
    		"		t.guide_id = g.id\r\n" + 
    		"		and t.status = 'belirlenecek'\r\n" + 
    		"		and (? between t.start_date and t.end_date\r\n" + 
    		"		or ? between t.start_date and t.end_date\r\n" + 
    		"		or t.start_date between ? and ?\r\n" + 
    		"		or t.end_date between ? and ?))\r\n" + 
    		"	and exists(\r\n" + 
    		"	select\r\n" + 
    		"		1\r\n" + 
    		"	from\r\n" + 
    		"		guide_price gp,\r\n" + 
    		"		place p\r\n" + 
    		"	where\r\n" + 
    		"		gp.guide_id = g.id\r\n" + 
    		"		and p.id = gp.place_id\r\n" + 
    		"		and (gp.guiding_price >= ?\r\n" + 
    		"		or ? is null)\r\n" + 
    		"		and (gp.guiding_price <= ?\r\n" + 
    		"		or ? is null))\r\n" + 
    		"	and ( g.gender = ?\r\n" + 
    		"	or ? = '2')\r\n" + 
    		"	and g.driver_licence = ?\r\n" + 
    		"	and gl.language_id = any (?)";

    private IndexSearchSchemaSql() {
    }

}

package com.rentaguide.repository.SQL;

public final class LanguagePageSchemaSql {
	
	public static final String SQL_LANG_PAGE_SAVE = "INSERT INTO LANGUAGE_PAGE (language_id, language_name, language_desc) VALUES (?,?,?)";
	public static final String SQL_LANG_PAGE_DELETE = "DELETE FROM LANGUAGE_PAGE WHERE language_id = ?";
    public static final String SQL_LANG_PAGE_GET_ALL = "SELECT * FROM LANGUAGE_PAGE";
    public static final String SQL_LANG_PAGE_GET_ONE = "SELECT * FROM LANGUAGE_PAGE WHERE language_id = ?";
    public static final String SQL_LANG_PAGE_UPDATE = "UPDATE LANGUAGE_PAGE SET language_id = ?, language_name = ?, language_desc = ?WHERE language_id = ?";

    private LanguagePageSchemaSql() {
    	
    }

}

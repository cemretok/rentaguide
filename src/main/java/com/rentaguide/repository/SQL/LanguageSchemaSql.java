package com.rentaguide.repository.SQL;

public final class LanguageSchemaSql {

    public static final String SQL_LANGUAGE_SAVE = "INSERT INTO LANGUAGE (name) values (?)";
    public static final String SQL_LANGUAGE_DELETE = "DELETE FROM LANGUAGE WHERE id = ?";
    public static final String SQL_LANGUAGE_GET_ALL = "SELECT * FROM LANGUAGE";
    public static final String SQL_LANGUAGE_GET_ONE = "SELECT * FROM LANGUAGE WHERE id = ?";
    public static final String SQL_LANGUAGE_UPDATE = "UPDATE LANGUAGE SET name = ? WHERE id = ?";

    private LanguageSchemaSql() {
    }

}

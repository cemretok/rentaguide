package com.rentaguide.repository.SQL;

public final class PaymentCreationSchemaSql {
	
	public static final String SQL_PAYMENT_SAVE = "INSERT INTO PAYMENT_CREATION (reservation_id,card_holder, card_number, expiration_month, expiration_year, cvc_number) values (?,?,?,?,?,?)";
    public static final String SQL_PAYMENT_DELETE = "DELETE FROM PAYMENT_CREATION WHERE id = ?";
    public static final String SQL_PAYMENT_GET_ALL = "SELECT * FROM PAYMENT_CREATION";
    public static final String SQL_PAYMENT_GET_ONE = "SELECT * FROM PAYMENT_CREATION WHERE id = ?";
    public static final String SQL_PAYMENT_UPDATE = "UPDATE PAYMENT_CREATION SET reservation_id = ?, card_holder = ?, card_number = ? , expiration_month = ?, expiration_year = ?, cvc_number = ? WHERE id = ?";

    private PaymentCreationSchemaSql() {
    }

}

package com.rentaguide.repository.SQL;

public final class PaymentResponseSchemaSql {
	
	public static final String SQL_PAYMENT_SAVE = "INSERT INTO PAYMENT_RESPONSE (status, description) values (?,?)";
    public static final String SQL_PAYMENT_DELETE = "DELETE FROM PAYMENT_RESPONSE WHERE id = ?";
    public static final String SQL_PAYMENT_GET_ALL = "SELECT * FROM PAYMENT_RESPONSE";
    public static final String SQL_PAYMENT_GET_ONE = "SELECT * FROM PAYMENT_RESPONSE WHERE id = ?";
    public static final String SQL_PAYMENT_UPDATE = "UPDATE PAYMENT_RESPONSE SET status = ?, description = ? WHERE id = ?";

    private PaymentResponseSchemaSql() {
    }

}

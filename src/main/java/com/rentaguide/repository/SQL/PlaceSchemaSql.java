package com.rentaguide.repository.SQL;

public final class PlaceSchemaSql {
    public static final String SQL_PLACE_SAVE = "INSERT INTO PLACE (country,district,province) values (?,?,?)";
    public static final String SQL_PLACE_DELETE = "DELETE FROM PLACE WHERE id = ?";
    public static final String SQL_PLACE_GET_ALL = "SELECT * FROM PLACE";
    public static final String SQL_PLACE_GET_ONE = "SELECT * FROM PLACE WHERE id = ?";
    public static final String SQL_PLACE_UPDATE = "UPDATE PLACE SET country = ?,district=?,province=? WHERE id = ?";

    private PlaceSchemaSql() {
    }

}

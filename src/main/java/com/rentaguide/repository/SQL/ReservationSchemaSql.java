package com.rentaguide.repository.SQL;

public final class ReservationSchemaSql {
    public static final String SQL_RESERVATION_SAVE = "INSERT INTO RESERVATION (guide_id,user_id,place_id,aim_id,start_date,end_date,total_price,reservation_status,notes) values (?,?,?,?,?,?,?,?,?)";
    public static final String SQL_RESERVATION_DELETE = "DELETE FROM RESERVATION WHERE id = ?";
    public static final String SQL_RESERVATION_GET_ALL_RESERVATIONS = "SELECT * FROM RESERVATION";
    public static final String SQL_RESERVATION_GET_ALL_USER_ID = "SELECT * FROM RESERVATION where user_id = ?";
    public static final String SQL_RESERVATION_GET_ALL_GUIDE_ID = "SELECT * FROM RESERVATION where guide_id = ?";
    public static final String SQL_RESERVATION_GET_ONE = "SELECT * FROM RESERVATION WHERE id = ?";
    public static final String SQL_RESERVATION_UPDATE = "UPDATE RESERVATION SET total_price =?,reservation_status =?,notes=? WHERE id = ?";

    private ReservationSchemaSql() {
    }

}

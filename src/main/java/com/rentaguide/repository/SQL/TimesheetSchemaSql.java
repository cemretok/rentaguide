package com.rentaguide.repository.SQL;

public final class TimesheetSchemaSql {
	
	public static final String SQL_TIMESHEET_SAVE = "INSERT INTO TIMESHEET (guide_id,place_id,start_date,end_date,status) values (?,?,?,?,?)";
    public static final String SQL_TIMESHEET_DELETE = "DELETE FROM TIMESHEET WHERE id = ?";
    public static final String SQL_TIMESHEET_GET_ALL = "SELECT * FROM TIMESHEET";
    public static final String SQL_TIMESHEET_ONE = "SELECT * FROM TIMESHEET WHERE id = ?";
    public static final String SQL_TIMESHEET_UPDATE = "UPDATE TIMESHEET SET status = ? WHERE id = ?";

    private TimesheetSchemaSql() {
    	
    }

}

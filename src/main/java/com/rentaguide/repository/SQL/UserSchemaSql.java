package com.rentaguide.repository.SQL;

public final class UserSchemaSql {

    public static final String SQL_USER_SAVE = "INSERT INTO USERS (mail,name,passport_no,password,phone,register_date,surname) values (?,?,?,?,?,?,?)";
    public static final String SQL_USER_DELETE = "DELETE FROM USERS WHERE id = ?";
    public static final String SQL_USER_FIND = "SELECT * FROM USERS WHERE id = ? or mail = ? or passport_no = ?";
    public static final String SQL_USER_GET_ONE = "SELECT * FROM USERS WHERE id = ?";
    public static final String SQL_USER_UPDATE = "UPDATE USERS SET name = ?, surname = ?, passport_no= ?, phone = ? WHERE id = ?";

    private UserSchemaSql() {
    }

}

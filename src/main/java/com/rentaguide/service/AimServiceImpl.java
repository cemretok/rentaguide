package com.rentaguide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rentaguide.entity.Aim;
import com.rentaguide.repository.DB.AimDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;

@Service
public class AimServiceImpl implements SimpleDao<Aim> {

    private AimDaoImpl dao;

    private AimServiceImpl(AimDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public List<Aim> getAll() {
        return dao.getAll();
    }

    @Override
    public Aim getOne(Aim aim) {
        return dao.getOne(aim);
    }

    @Override
    public int save(Aim aim) {
        return dao.save(aim);
    }

    @Override
    public int update(Aim aim) {
        return dao.update(aim);
    }

    @Override
    public void delete(Aim aim) {
        dao.delete(aim);
    }
}

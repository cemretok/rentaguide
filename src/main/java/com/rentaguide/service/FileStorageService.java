package com.rentaguide.service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageService {
	
	private final String imagePath = System.getProperty("user.dir") + "/images";
	private final Path rootLocation = Paths.get(imagePath);
	 
	  public void store(MultipartFile file, String id) {
	    try {
	    	/*
	    	System.out.println("id =" +id);
	    	String imageName = StringUtils.cleanPath(file.getOriginalFilename());
	    	System.out.println("imageName    "+imageName);
	    	imageName = imageName.replace(" ", id);
	    	System.out.println("imageName "+ imageName);
	    	*/
	    	
	    	Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename().replaceAll(file.getOriginalFilename(), id)));
	    	//Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
	    } catch (Exception e) {
	    	System.out.println(e.getMessage());
	    	System.out.println("work dir "+ System.getProperty("user.dir"));
	      throw new RuntimeException("FAIL!");
	    }
	  }
	 
	  public Resource loadFile(String filename) {
	    try {
	      Path file = rootLocation.resolve(filename);
	      Resource resource = new UrlResource(file.toUri());
	      if (resource.exists() || resource.isReadable()) {
	        return resource;
	      } else {
	        throw new RuntimeException("FAIL!");
	      }
	    } catch (MalformedURLException e) {
	      throw new RuntimeException("FAIL!");
	    }
	  }
	 
	  public void deleteAll() {
	    FileSystemUtils.deleteRecursively(rootLocation.toFile());
	  }
	 
	  public void init() {
	    try {
	      Files.createDirectory(rootLocation);
	    } catch (IOException e) {
	      throw new RuntimeException("Could not initialize storage!");
	    }
	  }
}

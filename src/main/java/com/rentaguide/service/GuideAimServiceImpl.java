package com.rentaguide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rentaguide.entity.GuideAim;
import com.rentaguide.repository.DB.GuideAimDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;

@Service
public class GuideAimServiceImpl implements SimpleDao<GuideAim> {

    private GuideAimDaoImpl dao;

    private GuideAimServiceImpl(GuideAimDaoImpl dao) {
        this.dao = dao;
    }


    @Override
    public List<GuideAim> getAll() {
        return dao.getAll();
    }

    @Override
    public GuideAim getOne(GuideAim guideAim) {
        return dao.getOne(guideAim);
    }

    @Override
    public int save(GuideAim guideAim) {
        return dao.save(guideAim);
    }

    @Override
    public int update(GuideAim guideAim) {
        return dao.update(guideAim);
    }

    @Override
    public void delete(GuideAim guideAim) {
        dao.delete(guideAim);
    }

}

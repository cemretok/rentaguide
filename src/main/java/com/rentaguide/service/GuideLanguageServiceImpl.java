package com.rentaguide.service;

import com.rentaguide.entity.GuideLanguage;
import com.rentaguide.repository.DB.GuideLanguageDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuideLanguageServiceImpl implements SimpleDao<GuideLanguage> {

    private GuideLanguageDaoImpl dao;

    private GuideLanguageServiceImpl(GuideLanguageDaoImpl dao) {
        this.dao = dao;
    }


    @Override
    public List<GuideLanguage> getAll() {
        return dao.getAll();
    }

    @Override
    public GuideLanguage getOne(GuideLanguage GuideLanguage) {
        return dao.getOne(GuideLanguage);
    }

    @Override
    public int save(GuideLanguage GuideLanguage) {
        return dao.save(GuideLanguage);
    }

    @Override
    public int update(GuideLanguage GuideLanguage) {
        return dao.update(GuideLanguage);
    }

    @Override
    public void delete(GuideLanguage GuideLanguage) {
        dao.delete(GuideLanguage);
    }
}

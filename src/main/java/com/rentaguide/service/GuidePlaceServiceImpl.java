package com.rentaguide.service;

import com.rentaguide.entity.GuidePlace;
import com.rentaguide.repository.DB.GuidePlaceDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuidePlaceServiceImpl implements SimpleDao<GuidePlace> {

    private GuidePlaceDaoImpl dao;

    private GuidePlaceServiceImpl(GuidePlaceDaoImpl dao) {
        this.dao = dao;
    }


    @Override
    public List<GuidePlace> getAll() {
        return dao.getAll();
    }

    @Override
    public GuidePlace getOne(GuidePlace guidePlace) {
        return dao.getOne(guidePlace);
    }

    @Override
    public int save(GuidePlace guidePlace) {
        return dao.save(guidePlace);
    }

    @Override
    public int update(GuidePlace guidePlace) {
        return dao.update(guidePlace);
    }

    @Override
    public void delete(GuidePlace guidePlace) {
        dao.delete(guidePlace);
    }
}

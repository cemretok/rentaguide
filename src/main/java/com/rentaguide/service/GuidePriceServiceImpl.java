package com.rentaguide.service;

import com.rentaguide.entity.GuidePrice;
import com.rentaguide.repository.DB.GuidePriceDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuidePriceServiceImpl implements SimpleDao<GuidePrice> {

    private GuidePriceDaoImpl dao;

    private GuidePriceServiceImpl(GuidePriceDaoImpl dao) {
        this.dao = dao;
    }


    @Override
    public List<GuidePrice> getAll() {
        return dao.getAll();
    }

    @Override
    public GuidePrice getOne(GuidePrice guidePrice) {
        return dao.getOne(guidePrice);
    }

    @Override
    public int save(GuidePrice guidePrice) {
        return dao.save(guidePrice);
    }

    @Override
    public int update(GuidePrice guidePrice) {
        return dao.update(guidePrice);
    }

    @Override
    public void delete(GuidePrice guidePrice) {
        dao.delete(guidePrice);
    }
}

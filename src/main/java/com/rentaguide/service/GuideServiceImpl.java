package com.rentaguide.service;

import com.rentaguide.entity.Guide;
import com.rentaguide.repository.DB.GuideDaoImpl;
import com.rentaguide.repository.DB.Interfaces.UserDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuideServiceImpl implements UserDao<Guide> {

    private GuideDaoImpl dao;

    private GuideServiceImpl(GuideDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public int save(Guide guide) {
        return dao.save(guide);
    }

    @Override
    public void delete(Guide guide) {
        dao.delete(guide);
    }

    @Override
    public int update(Guide guide) {
        return dao.update(guide);
    }

    @Override
    public List<Guide> find(Guide guide) {
        return dao.find(guide);
    }

    @Override
    public Guide getOne(Guide guide) {
        return dao.getOne(guide);
    }

    @Override
    public Guide findByMail(String mail) {
        return dao.findByMail(mail);
    }
}

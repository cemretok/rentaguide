package com.rentaguide.service;

import com.rentaguide.entity.IndexSearch;
import com.rentaguide.repository.DB.IndexSearchDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SearchDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexSearchServiceImpl implements SearchDao<IndexSearch> {

    private IndexSearchDaoImpl dao;

    private IndexSearchServiceImpl(IndexSearchDaoImpl dao) {
        this.dao = dao;
    }


    @Override
    public List<IndexSearch> find(IndexSearch indexSearch) {
        return dao.find(indexSearch);
    }

    @Override
    public IndexSearch getOne(IndexSearch indexSearch) {
        return dao.getOne(indexSearch);
    }

    @Override
    public int save(IndexSearch indexSearch) {
        return dao.save(indexSearch);
    }

    @Override
    public int update(IndexSearch indexSearch) {
        return dao.update(indexSearch);
    }

    @Override
    public void delete(IndexSearch indexSearch) {
        dao.delete(indexSearch);
    }
}

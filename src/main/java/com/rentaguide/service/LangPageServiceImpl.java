package com.rentaguide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rentaguide.entity.LanguagePage;
import com.rentaguide.repository.DB.LangPageDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;

@Service
public class LangPageServiceImpl implements SimpleDao<LanguagePage> {
	
	private LangPageDaoImpl dao;
	
	public LangPageServiceImpl(LangPageDaoImpl dao) {
		this.dao = dao;
	}

	@Override
	public List<LanguagePage> getAll() {
		return dao.getAll();
	}

	@Override
	public LanguagePage getOne(LanguagePage t) {
		return dao.getOne(t);
	}

	@Override
	public int save(LanguagePage t) {
		return dao.save(t);
	}

	@Override
	public int update(LanguagePage t) {
		return dao.update(t);
	}

	@Override
	public void delete(LanguagePage t) {
		dao.delete(t);
	}

}

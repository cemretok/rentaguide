package com.rentaguide.service;

import com.rentaguide.entity.Language;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.DB.LanguageDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageServiceImpl implements SimpleDao<Language> {

    private LanguageDaoImpl dao;

    private LanguageServiceImpl(LanguageDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public List<Language> getAll() {
        return dao.getAll();
    }

    @Override
    public Language getOne(Language language) {
        return dao.getOne(language);
    }

    @Override
    public int save(Language language) {
        return dao.save(language);
    }

    @Override
    public int update(Language language) {
        return dao.update(language);
    }

    @Override
    public void delete(Language language) {
        dao.delete(language);
    }
}

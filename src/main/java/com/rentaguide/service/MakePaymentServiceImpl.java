package com.rentaguide.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.iyzipay.Options;
import com.iyzipay.model.Address;
import com.iyzipay.model.BasketItem;
import com.iyzipay.model.BasketItemType;
import com.iyzipay.model.Buyer;
import com.iyzipay.model.Currency;
import com.iyzipay.model.Locale;
import com.iyzipay.model.Payment;
import com.iyzipay.model.PaymentCard;
import com.iyzipay.model.PaymentChannel;
import com.iyzipay.model.PaymentGroup;
import com.iyzipay.request.CreatePaymentRequest;
import com.rentaguide.entity.PaymentCreation;
import com.rentaguide.entity.PaymentResponse;
import com.rentaguide.entity.Reservation;

@Service
public class MakePaymentServiceImpl {
	
	public PaymentResponse makePayment(PaymentCreation pc, Reservation userGuideReservation) {
		
		Options options = new Options();
		options.setApiKey("sandbox-lCXvsuG6nascOL91ciQtmPxqWnVzyYsw");
		options.setSecretKey("sandbox-KsR9cR2f5w32jXprTKnHaWI3Y5FBD4Om");
		options.setBaseUrl("https://sandbox-api.iyzipay.com");
		
		CreatePaymentRequest request = new CreatePaymentRequest();
		request.setLocale(Locale.TR.getValue());
		request.setConversationId(pc.getReservationId()+"");
		request.setPrice(new BigDecimal(userGuideReservation.getTotalPrice()));
		request.setPaidPrice(new BigDecimal(userGuideReservation.getTotalPrice()));
		request.setCurrency(Currency.TRY.name());
		request.setInstallment(1);
		request.setBasketId(pc.getReservationId()+"");
		request.setPaymentChannel(PaymentChannel.WEB.name());
		request.setPaymentGroup(PaymentGroup.PRODUCT.name());

		PaymentCard paymentCard = new PaymentCard();
		paymentCard.setCardHolderName(pc.getCardHolder());
		paymentCard.setCardNumber(pc.getCardNumber());
		paymentCard.setExpireMonth(pc.getExpirationMonth());
		paymentCard.setExpireYear(pc.getExpirationYear());
		paymentCard.setCvc(pc.getCvcNumber());
		paymentCard.setRegisterCard(0);
		request.setPaymentCard(paymentCard);

		Buyer buyer = new Buyer();
		buyer.setId(userGuideReservation.getUser().getId().toString());
		buyer.setName(userGuideReservation.getUser().getName());
		buyer.setSurname(userGuideReservation.getUser().getName());
		buyer.setGsmNumber(userGuideReservation.getUser().getPhone());
		buyer.setEmail(userGuideReservation.getUser().getMail());
		buyer.setIdentityNumber(userGuideReservation.getUser().getPassportNo());
		buyer.setLastLoginDate(getDatetimeFormatter(LocalDateTime.now()));
		buyer.setRegistrationDate(getDatetimeFormatter(userGuideReservation.getGuide().getRegisterDate()));
		
		
		
		buyer.setRegistrationAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
		//buyer.setIp("85.34.78.112");
		buyer.setCity("Istanbul");
		buyer.setCountry("Turkey");
		//buyer.setZipCode("34732");
		
		request.setBuyer(buyer);

		//TODO
		Address shippingAddress = new Address();
		shippingAddress.setContactName("Ali Kaplan");
		shippingAddress.setCity("Istanbul");
		shippingAddress.setCountry("Turkey");
		shippingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
		shippingAddress.setZipCode("34742");
		request.setShippingAddress(shippingAddress);
	

		//TODO
		Address billingAddress = new Address();
		billingAddress.setContactName("Jane Doe");
		billingAddress.setCity("Istanbul");
		billingAddress.setCountry("Turkey");
		billingAddress.setAddress("Nidakule Göztepe, Merdivenköy Mah. Bora Sok. No:1");
		billingAddress.setZipCode("34742");
		request.setBillingAddress(billingAddress);

		List<BasketItem> basketItems = new ArrayList<BasketItem>();
		BasketItem firstBasketItem = new BasketItem();
		firstBasketItem.setId(userGuideReservation.getGuide().getId().toString());
		firstBasketItem.setName(userGuideReservation.getGuide().getName());
		firstBasketItem.setCategory1(userGuideReservation.getPlace().getCountry());
		firstBasketItem.setCategory2(userGuideReservation.getPlace().getDistrict());
		firstBasketItem.setItemType(BasketItemType.PHYSICAL.name());
		firstBasketItem.setPrice(new BigDecimal(userGuideReservation.getTotalPrice()));
		basketItems.add(firstBasketItem);
		
		request.setBasketItems(basketItems);

		Payment payment = Payment.create(request, options);
		PaymentResponse pr = new PaymentResponse();
		pr.setDescription(payment.getErrorMessage());
		pr.setStatus(payment.getStatus());
		return pr;
		
		//return Payment.create(request, options);
			
		
	}
	
	public String getDatetimeFormatter(LocalDateTime date){
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String formatDateTime = date.format(formatter);

		
        return formatDateTime;
	}
	

}

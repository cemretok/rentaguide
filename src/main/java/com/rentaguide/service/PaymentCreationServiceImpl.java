package com.rentaguide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rentaguide.entity.PaymentCreation;
import com.rentaguide.entity.PaymentResponse;
import com.rentaguide.entity.Reservation;
import com.rentaguide.repository.DB.PaymentCreationDaoImpl;
import com.rentaguide.repository.DB.ReservationDaoImpl;
import com.rentaguide.repository.DB.Interfaces.PaymentDao;

@Service
public class PaymentCreationServiceImpl implements PaymentDao<PaymentCreation>{
	
	private PaymentCreationDaoImpl dao;
	private MakePaymentServiceImpl makePaymentService;
	private ReservationDaoImpl reservationDao;
	
	private PaymentCreationServiceImpl (PaymentCreationDaoImpl dao, MakePaymentServiceImpl makePaymentService,ReservationDaoImpl reservationDao) {
		this.dao = dao;
		this.makePaymentService = makePaymentService;
		this.reservationDao = reservationDao;
	}
	
	@Override
	public List<PaymentCreation> getAll() {
		return dao.getAll();
	}

	@Override
	public PaymentCreation getOne(PaymentCreation pc) {
		return dao.getOne(pc);
	}

	@Override
	public int save(PaymentCreation pc) {
		return dao.save(pc);
	}

	@Override
	public int update(PaymentCreation pc) {
		return dao.update(pc);
	}

	@Override
	public void delete(PaymentCreation pc) {
		dao.delete(pc);
		
	}
	
	@Override
	public PaymentResponse makePayment(PaymentCreation pc) {
		Reservation userGuideReservation = new Reservation();
		userGuideReservation.setId(pc.getReservationId());
		userGuideReservation = reservationDao.getOne(userGuideReservation);
		return makePaymentService.makePayment(pc,userGuideReservation);
	}
	
	

}

package com.rentaguide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rentaguide.entity.PaymentResponse;
import com.rentaguide.repository.DB.PaymentResponseDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;

@Service
public class PaymentRespomseServiceImpl implements SimpleDao<PaymentResponse>{
	
	private PaymentResponseDaoImpl dao;
	
	private PaymentRespomseServiceImpl (PaymentResponseDaoImpl dao) {
		this.dao = dao;
	}

	@Override
	public List<PaymentResponse> getAll() {
		return dao.getAll();
	}

	@Override
	public PaymentResponse getOne(PaymentResponse t) {
		return dao.getOne(t);
	}

	@Override
	public int save(PaymentResponse t) {
		return dao.save(t);
	}

	@Override
	public int update(PaymentResponse t) {
		return dao.update(t);
	}

	@Override
	public void delete(PaymentResponse t) {
		dao.delete(t);		
	}
	
	

}

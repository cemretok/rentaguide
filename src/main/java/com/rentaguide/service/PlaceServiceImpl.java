package com.rentaguide.service;

import com.rentaguide.entity.Place;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;
import com.rentaguide.repository.DB.PlaceDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaceServiceImpl implements SimpleDao<Place> {

    private PlaceDaoImpl dao;

    private PlaceServiceImpl(PlaceDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public List<Place> getAll() {
        return dao.getAll();
    }

    @Override
    public Place getOne(Place place) {
        return dao.getOne(place);
    }

    @Override
    public int save(Place place) {
        return dao.save(place);
    }

    @Override
    public int update(Place place) {
        return dao.update(place);
    }

    @Override
    public void delete(Place place) {
        dao.delete(place);
    }
}

package com.rentaguide.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.rentaguide.entity.Reservation;
import com.rentaguide.repository.DB.ReservationDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;

@Service
public class ReservationServiceImpl implements SimpleDao<Reservation> {

    private ReservationDaoImpl dao;

    private ReservationServiceImpl(ReservationDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public List<Reservation> getAll() {
        return dao.getAll();
    }

    @Override
    public Reservation getOne(Reservation reservation) {
        return dao.getOne(reservation);
    }

    @Override
    public int save(Reservation reservation) {
        return dao.save(reservation);
    }

    @Override
    public int update(Reservation reservation) {
        return dao.update(reservation);
    }

    @Override
    public void delete(Reservation reservation) {
        dao.delete(reservation);
    }

    public List<Reservation> getAllUsers(Integer userId) {
        return dao.getAllForUsers(userId);
    }
    
    public List<Reservation> getAllGuides(Integer guideId) {
        return dao.getAllForGuides(guideId);
    }
}

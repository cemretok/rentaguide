package com.rentaguide.service;

import java.util.List;

import com.rentaguide.entity.Timesheet;
import com.rentaguide.repository.DB.TimesheetDaoImpl;
import com.rentaguide.repository.DB.Interfaces.SimpleDao;

import org.springframework.stereotype.Service;

@Service
public class TimesheetServiceImpl implements SimpleDao<Timesheet> {

	private TimesheetDaoImpl dao;

	private TimesheetServiceImpl(TimesheetDaoImpl dao) {
		this.dao = dao;
	}

	@Override
	public List<Timesheet> getAll() {
		return dao.getAll();
	}

	@Override
	public Timesheet getOne(Timesheet timesheet) {
		return dao.getOne(timesheet);
	}

	@Override
	public int save(Timesheet timesheet) {
		return dao.save(timesheet);
	}

	@Override
	public int update(Timesheet timesheet) {
		return dao.update(timesheet);
	}

	@Override
	public void delete(Timesheet timesheet) {
		dao.delete(timesheet);

	}

}

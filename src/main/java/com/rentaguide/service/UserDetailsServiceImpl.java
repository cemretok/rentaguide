package com.rentaguide.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyList;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserServiceImpl userService;
    private GuideServiceImpl guideService;

    public UserDetailsServiceImpl(UserServiceImpl userService, GuideServiceImpl guideService) {
        this.userService = userService;
        this.guideService = guideService;
    }

    @Override
    public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
        com.rentaguide.entity.User user = userService.findByMail(mail);

        if (user != null)
            return new User(user.getMail(), user.getPassword(), emptyList());

        com.rentaguide.entity.Guide guide = guideService.findByMail(mail);

        if (guide != null)
            return new User(guide.getMail(), guide.getPassword(), emptyList());

        if (guide == null && user == null)
            throw new UsernameNotFoundException(null);

        return new User(null, null, emptyList());
    }
}
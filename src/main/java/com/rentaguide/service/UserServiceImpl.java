package com.rentaguide.service;

import com.rentaguide.entity.User;
import com.rentaguide.repository.DB.Interfaces.UserDao;
import com.rentaguide.repository.DB.UserDaoImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserDao<User> {

    private UserDaoImpl dao;

    private UserServiceImpl(UserDaoImpl dao) {
        this.dao = dao;
    }

    @Override
    public int save(User user) {
        return dao.save(user);
    }

    @Override
    public void delete(User user) {
        dao.delete(user);
    }

    @Override
    public int update(User user) {
        return dao.update(user);
    }

    @Override
    public List<User> find(User user) {
        return dao.find(user);
    }

    @Override
    public User getOne(User user) {
        return dao.getOne(user);
    }

    public User findByMail(String mail) {
        return dao.findByMail(mail);
    }

}
